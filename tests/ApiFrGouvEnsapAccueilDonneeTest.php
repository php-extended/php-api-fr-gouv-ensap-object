<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-ensap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvEnsap\Test;

use PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapAccueilDonnee;
use PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapEvenement;
use PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapIdentification;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrGouvEnsapAccueilDonneeTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapAccueilDonnee
 * @internal
 * @small
 */
class ApiFrGouvEnsapAccueilDonneeTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrGouvEnsapAccueilDonnee
	 */
	protected ApiFrGouvEnsapAccueilDonnee $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetIdentification() : void
	{
		$this->assertEquals($this->getMockBuilder(ApiFrGouvEnsapIdentification::class)->disableOriginalConstructor()->getMock(), $this->_object->getIdentification());
		$expected = $this->getMockBuilder(ApiFrGouvEnsapIdentification::class)->disableOriginalConstructor()->getMock();
		$this->_object->setIdentification($expected);
		$this->assertEquals($expected, $this->_object->getIdentification());
	}
	
	public function testGetListeEvenement() : void
	{
		$this->assertEquals([$this->getMockBuilder(ApiFrGouvEnsapEvenement::class)->disableOriginalConstructor()->getMock()], $this->_object->getListeEvenement());
		$expected = [$this->getMockBuilder(ApiFrGouvEnsapEvenement::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrGouvEnsapEvenement::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setListeEvenement($expected);
		$this->assertEquals($expected, $this->_object->getListeEvenement());
	}
	
	public function testGetListeAnneeRemuneration() : void
	{
		$this->assertEquals([12], $this->_object->getListeAnneeRemuneration());
		$expected = [25, 25];
		$this->_object->setListeAnneeRemuneration($expected);
		$this->assertEquals($expected, $this->_object->getListeAnneeRemuneration());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrGouvEnsapAccueilDonnee($this->getMockBuilder(ApiFrGouvEnsapIdentification::class)->disableOriginalConstructor()->getMock(), [$this->getMockBuilder(ApiFrGouvEnsapEvenement::class)->disableOriginalConstructor()->getMock()], [12]);
	}
	
}
