<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-ensap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvEnsap\Test;

use PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapProfileDonnee;
use PhpExtended\Email\EmailAddressParser;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrGouvEnsapProfileDonneeTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapProfileDonnee
 * @internal
 * @small
 */
class ApiFrGouvEnsapProfileDonneeTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrGouvEnsapProfileDonnee
	 */
	protected ApiFrGouvEnsapProfileDonnee $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testHasEstAbonneNotification() : void
	{
		$this->assertFalse($this->_object->hasEstAbonneNotification());
		$expected = true;
		$this->_object->setEstAbonneNotification($expected);
		$this->assertTrue($this->_object->hasEstAbonneNotification());
	}
	
	public function testHasEstAbonneMailEnsap() : void
	{
		$this->assertNull($this->_object->hasEstAbonneMailEnsap());
		$expected = true;
		$this->_object->setEstAbonneMailEnsap($expected);
		$this->assertTrue($this->_object->hasEstAbonneMailEnsap());
	}
	
	public function testGetMailNotification() : void
	{
		$this->assertNull($this->_object->getMailNotification());
		$expected = (new EmailAddressParser())->parse('admin@example.com');
		$this->_object->setMailNotification($expected);
		$this->assertEquals($expected, $this->_object->getMailNotification());
	}
	
	public function testGetMailPrincipal() : void
	{
		$this->assertEquals((new EmailAddressParser())->parse('test@example.com'), $this->_object->getMailPrincipal());
		$expected = (new EmailAddressParser())->parse('admin@example.com');
		$this->_object->setMailPrincipal($expected);
		$this->assertEquals($expected, $this->_object->getMailPrincipal());
	}
	
	public function testGetMailSecondaire() : void
	{
		$this->assertEquals((new EmailAddressParser())->parse('test@example.com'), $this->_object->getMailSecondaire());
		$expected = (new EmailAddressParser())->parse('admin@example.com');
		$this->_object->setMailSecondaire($expected);
		$this->assertEquals($expected, $this->_object->getMailSecondaire());
	}
	
	public function testGetModeInscriptionUsager() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getModeInscriptionUsager());
		$expected = 'qsdfghjklm';
		$this->_object->setModeInscriptionUsager($expected);
		$this->assertEquals($expected, $this->_object->getModeInscriptionUsager());
	}
	
	public function testGetNir() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getNir());
		$expected = 'qsdfghjklm';
		$this->_object->setNir($expected);
		$this->assertEquals($expected, $this->_object->getNir());
	}
	
	public function testGetNom() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getNom());
		$expected = 'qsdfghjklm';
		$this->_object->setNom($expected);
		$this->assertEquals($expected, $this->_object->getNom());
	}
	
	public function testGetPrenom() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getPrenom());
		$expected = 'qsdfghjklm';
		$this->_object->setPrenom($expected);
		$this->assertEquals($expected, $this->_object->getPrenom());
	}
	
	public function testGetCodeSexe() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getCodeSexe());
		$expected = 'qsdfghjklm';
		$this->_object->setCodeSexe($expected);
		$this->assertEquals($expected, $this->_object->getCodeSexe());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrGouvEnsapProfileDonnee(false, (new EmailAddressParser())->parse('test@example.com'), (new EmailAddressParser())->parse('test@example.com'), 'azertyuiop', 'azertyuiop', 'azertyuiop', 'azertyuiop', 'azertyuiop');
	}
	
}
