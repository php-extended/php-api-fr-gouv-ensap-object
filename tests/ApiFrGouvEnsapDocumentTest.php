<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-ensap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvEnsap\Test;

use DateTimeImmutable;
use PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapDocument;
use PhpExtended\Uuid\UuidParser;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrGouvEnsapDocumentTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapDocument
 * @internal
 * @small
 */
class ApiFrGouvEnsapDocumentTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrGouvEnsapDocument
	 */
	protected ApiFrGouvEnsapDocument $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetDocumentUuid() : void
	{
		$this->assertEquals((new UuidParser())->parse('9e2cfea1-afc9-4ecb-8a90-d477a5a90cfa'), $this->_object->getDocumentUuid());
		$expected = (new UuidParser())->parse('b592ccab-7e43-4de9-8460-34592395addb');
		$this->_object->setDocumentUuid($expected);
		$this->assertEquals($expected, $this->_object->getDocumentUuid());
	}
	
	public function testGetCodeSousThemeGed() : void
	{
		$this->assertNull($this->_object->getCodeSousThemeGed());
		$expected = 'qsdfghjklm';
		$this->_object->setCodeSousThemeGed($expected);
		$this->assertEquals($expected, $this->_object->getCodeSousThemeGed());
	}
	
	public function testGetLibelle1() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getLibelle1());
		$expected = 'qsdfghjklm';
		$this->_object->setLibelle1($expected);
		$this->assertEquals($expected, $this->_object->getLibelle1());
	}
	
	public function testGetLibelle2() : void
	{
		$this->assertNull($this->_object->getLibelle2());
		$expected = 'qsdfghjklm';
		$this->_object->setLibelle2($expected);
		$this->assertEquals($expected, $this->_object->getLibelle2());
	}
	
	public function testGetLibelle3() : void
	{
		$this->assertNull($this->_object->getLibelle3());
		$expected = 'qsdfghjklm';
		$this->_object->setLibelle3($expected);
		$this->assertEquals($expected, $this->_object->getLibelle3());
	}
	
	public function testGetDateDocument() : void
	{
		$this->assertEquals(DateTimeImmutable::createFromFormat('!Y-m-d H:i:s', '2000-01-01 00:00:01'), $this->_object->getDateDocument());
		$expected = DateTimeImmutable::createFromFormat('!Y-m-d H:i:s', '2001-01-01 00:00:01');
		$this->_object->setDateDocument($expected);
		$this->assertEquals($expected, $this->_object->getDateDocument());
	}
	
	public function testGetAnnee() : void
	{
		$this->assertEquals(12, $this->_object->getAnnee());
		$expected = 25;
		$this->_object->setAnnee($expected);
		$this->assertEquals($expected, $this->_object->getAnnee());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrGouvEnsapDocument((new UuidParser())->parse('9e2cfea1-afc9-4ecb-8a90-d477a5a90cfa'), 'azertyuiop', DateTimeImmutable::createFromFormat('!Y-m-d H:i:s', '2000-01-01 00:00:01'), 12);
	}
	
}
