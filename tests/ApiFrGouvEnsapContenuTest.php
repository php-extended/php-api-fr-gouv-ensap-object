<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-ensap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvEnsap\Test;

use PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapContenu;
use PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapText;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrGouvEnsapContenuTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapContenu
 * @internal
 * @small
 */
class ApiFrGouvEnsapContenuTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrGouvEnsapContenu
	 */
	protected ApiFrGouvEnsapContenu $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetChamp() : void
	{
		$this->assertEquals(['key' => 'azertyuiop'], $this->_object->getChamp());
		$expected = ['key1' => 'qsdfghjklm', 'key2' => 'qsdfghjklm'];
		$this->_object->setChamp($expected);
		$this->assertEquals($expected, $this->_object->getChamp());
	}
	
	public function testGetParagraphe() : void
	{
		$this->assertEquals(['key' => $this->getMockBuilder(ApiFrGouvEnsapText::class)->disableOriginalConstructor()->getMock()], $this->_object->getParagraphe());
		$expected = ['key1' => $this->getMockBuilder(ApiFrGouvEnsapText::class)->disableOriginalConstructor()->getMock(), 'key2' => $this->getMockBuilder(ApiFrGouvEnsapText::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setParagraphe($expected);
		$this->assertEquals($expected, $this->_object->getParagraphe());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrGouvEnsapContenu(['key' => 'azertyuiop'], ['key' => $this->getMockBuilder(ApiFrGouvEnsapText::class)->disableOriginalConstructor()->getMock()]);
	}
	
}
