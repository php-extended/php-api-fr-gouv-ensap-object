<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-ensap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvEnsap\Test;

use DateTimeImmutable;
use PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapBulletin;
use PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapBulletinLine;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrGouvEnsapBulletinTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapBulletin
 * @internal
 * @small
 */
class ApiFrGouvEnsapBulletinTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrGouvEnsapBulletin
	 */
	protected ApiFrGouvEnsapBulletin $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetEmitter() : void
	{
		$this->assertNull($this->_object->getEmitter());
		$expected = 'qsdfghjklm';
		$this->_object->setEmitter($expected);
		$this->assertEquals($expected, $this->_object->getEmitter());
	}
	
	public function testGetMonthLetter() : void
	{
		$this->assertNull($this->_object->getMonthLetter());
		$expected = 'qsdfghjklm';
		$this->_object->setMonthLetter($expected);
		$this->assertEquals($expected, $this->_object->getMonthLetter());
	}
	
	public function testGetNumOrder() : void
	{
		$this->assertNull($this->_object->getNumOrder());
		$expected = 'qsdfghjklm';
		$this->_object->setNumOrder($expected);
		$this->assertEquals($expected, $this->_object->getNumOrder());
	}
	
	public function testGetWorkTime() : void
	{
		$this->assertNull($this->_object->getWorkTime());
		$expected = 'qsdfghjklm';
		$this->_object->setWorkTime($expected);
		$this->assertEquals($expected, $this->_object->getWorkTime());
	}
	
	public function testGetPoste1() : void
	{
		$this->assertNull($this->_object->getPoste1());
		$expected = 'qsdfghjklm';
		$this->_object->setPoste1($expected);
		$this->assertEquals($expected, $this->_object->getPoste1());
	}
	
	public function testGetPoste2() : void
	{
		$this->assertNull($this->_object->getPoste2());
		$expected = 'qsdfghjklm';
		$this->_object->setPoste2($expected);
		$this->assertEquals($expected, $this->_object->getPoste2());
	}
	
	public function testGetLibellePoste() : void
	{
		$this->assertNull($this->_object->getLibellePoste());
		$expected = 'qsdfghjklm';
		$this->_object->setLibellePoste($expected);
		$this->assertEquals($expected, $this->_object->getLibellePoste());
	}
	
	public function testGetSiretEmployeur() : void
	{
		$this->assertNull($this->_object->getSiretEmployeur());
		$expected = 'qsdfghjklm';
		$this->_object->setSiretEmployeur($expected);
		$this->assertEquals($expected, $this->_object->getSiretEmployeur());
	}
	
	public function testGetIdMin() : void
	{
		$this->assertNull($this->_object->getIdMin());
		$expected = 25;
		$this->_object->setIdMin($expected);
		$this->assertEquals($expected, $this->_object->getIdMin());
	}
	
	public function testGetIdNir() : void
	{
		$this->assertNull($this->_object->getIdNir());
		$expected = 'qsdfghjklm';
		$this->_object->setIdNir($expected);
		$this->assertEquals($expected, $this->_object->getIdNir());
	}
	
	public function testGetIdNodos() : void
	{
		$this->assertNull($this->_object->getIdNodos());
		$expected = 25;
		$this->_object->setIdNodos($expected);
		$this->assertEquals($expected, $this->_object->getIdNodos());
	}
	
	public function testGetGrade() : void
	{
		$this->assertNull($this->_object->getGrade());
		$expected = 'qsdfghjklm';
		$this->_object->setGrade($expected);
		$this->assertEquals($expected, $this->_object->getGrade());
	}
	
	public function testGetChildCharged() : void
	{
		$this->assertNull($this->_object->getChildCharged());
		$expected = 25;
		$this->_object->setChildCharged($expected);
		$this->assertEquals($expected, $this->_object->getChildCharged());
	}
	
	public function testGetEchelon() : void
	{
		$this->assertNull($this->_object->getEchelon());
		$expected = 'qsdfghjklm';
		$this->_object->setEchelon($expected);
		$this->assertEquals($expected, $this->_object->getEchelon());
	}
	
	public function testGetIndiceOrNbHours() : void
	{
		$this->assertNull($this->_object->getIndiceOrNbHours());
		$expected = 25;
		$this->_object->setIndiceOrNbHours($expected);
		$this->assertEquals($expected, $this->_object->getIndiceOrNbHours());
	}
	
	public function testGetTauxHoraire() : void
	{
		$this->assertNull($this->_object->getTauxHoraire());
		$expected = 25;
		$this->_object->setTauxHoraire($expected);
		$this->assertEquals($expected, $this->_object->getTauxHoraire());
	}
	
	public function testGetTpsPartiel() : void
	{
		$this->assertNull($this->_object->getTpsPartiel());
		$expected = 'qsdfghjklm';
		$this->_object->setTpsPartiel($expected);
		$this->assertEquals($expected, $this->_object->getTpsPartiel());
	}
	
	public function testGetLines() : void
	{
		$this->assertEquals([], $this->_object->getLines());
		$expected = [$this->getMockBuilder(ApiFrGouvEnsapBulletinLine::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrGouvEnsapBulletinLine::class)->disableOriginalConstructor()->getMock()];
		$this->_object->setLines($expected);
		$this->assertEquals($expected, $this->_object->getLines());
	}
	
	public function testGetTotalAPayer() : void
	{
		$this->assertNull($this->_object->getTotalAPayer());
		$expected = 25;
		$this->_object->setTotalAPayer($expected);
		$this->assertEquals($expected, $this->_object->getTotalAPayer());
	}
	
	public function testGetTotalADeduire() : void
	{
		$this->assertNull($this->_object->getTotalADeduire());
		$expected = 25;
		$this->_object->setTotalADeduire($expected);
		$this->assertEquals($expected, $this->_object->getTotalADeduire());
	}
	
	public function testGetTotalPourInfo() : void
	{
		$this->assertNull($this->_object->getTotalPourInfo());
		$expected = 25;
		$this->_object->setTotalPourInfo($expected);
		$this->assertEquals($expected, $this->_object->getTotalPourInfo());
	}
	
	public function testGetTotalEmployeur() : void
	{
		$this->assertNull($this->_object->getTotalEmployeur());
		$expected = 25;
		$this->_object->setTotalEmployeur($expected);
		$this->assertEquals($expected, $this->_object->getTotalEmployeur());
	}
	
	public function testGetTotalNetAPayer() : void
	{
		$this->assertNull($this->_object->getTotalNetAPayer());
		$expected = 25;
		$this->_object->setTotalNetAPayer($expected);
		$this->assertEquals($expected, $this->_object->getTotalNetAPayer());
	}
	
	public function testGetBaseSsYear() : void
	{
		$this->assertNull($this->_object->getBaseSsYear());
		$expected = 25;
		$this->_object->setBaseSsYear($expected);
		$this->assertEquals($expected, $this->_object->getBaseSsYear());
	}
	
	public function testGetBaseSsMonth() : void
	{
		$this->assertNull($this->_object->getBaseSsMonth());
		$expected = 25;
		$this->_object->setBaseSsMonth($expected);
		$this->assertEquals($expected, $this->_object->getBaseSsMonth());
	}
	
	public function testGetMontantImposableYear() : void
	{
		$this->assertNull($this->_object->getMontantImposableYear());
		$expected = 25;
		$this->_object->setMontantImposableYear($expected);
		$this->assertEquals($expected, $this->_object->getMontantImposableYear());
	}
	
	public function testGetMontahtImposableMonth() : void
	{
		$this->assertNull($this->_object->getMontahtImposableMonth());
		$expected = 25;
		$this->_object->setMontahtImposableMonth($expected);
		$this->assertEquals($expected, $this->_object->getMontahtImposableMonth());
	}
	
	public function testGetComptable() : void
	{
		$this->assertNull($this->_object->getComptable());
		$expected = 'qsdfghjklm';
		$this->_object->setComptable($expected);
		$this->assertEquals($expected, $this->_object->getComptable());
	}
	
	public function testGetDatePaid() : void
	{
		$this->assertNull($this->_object->getDatePaid());
		$expected = DateTimeImmutable::createFromFormat('!Y-m-d', '2001-01-01');
		$this->_object->setDatePaid($expected);
		$this->assertEquals($expected, $this->_object->getDatePaid());
	}
	
	public function testGetAccountPaidIban() : void
	{
		$this->assertNull($this->_object->getAccountPaidIban());
		$expected = 'qsdfghjklm';
		$this->_object->setAccountPaidIban($expected);
		$this->assertEquals($expected, $this->_object->getAccountPaidIban());
	}
	
	public function testGetAccountPaidBic() : void
	{
		$this->assertNull($this->_object->getAccountPaidBic());
		$expected = 'qsdfghjklm';
		$this->_object->setAccountPaidBic($expected);
		$this->assertEquals($expected, $this->_object->getAccountPaidBic());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrGouvEnsapBulletin();
	}
	
}
