<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-ensap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvEnsap\Test;

use PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapIdentification;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrGouvEnsapIdentificationTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapIdentification
 * @internal
 * @small
 */
class ApiFrGouvEnsapIdentificationTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrGouvEnsapIdentification
	 */
	protected ApiFrGouvEnsapIdentification $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetIdentite() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getIdentite());
		$expected = 'qsdfghjklm';
		$this->_object->setIdentite($expected);
		$this->assertEquals($expected, $this->_object->getIdentite());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrGouvEnsapIdentification('azertyuiop');
	}
	
}
