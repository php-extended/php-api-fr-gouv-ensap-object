<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-ensap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvEnsap\Test;

use PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapLoginResponse;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrGouvEnsapLoginResponseTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapLoginResponse
 * @internal
 * @small
 */
class ApiFrGouvEnsapLoginResponseTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrGouvEnsapLoginResponse
	 */
	protected ApiFrGouvEnsapLoginResponse $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetError() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getError());
		$expected = 'qsdfghjklm';
		$this->_object->setError($expected);
		$this->assertEquals($expected, $this->_object->getError());
	}
	
	public function testGetId() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getId());
		$expected = 'qsdfghjklm';
		$this->_object->setId($expected);
		$this->assertEquals($expected, $this->_object->getId());
	}
	
	public function testGetCode() : void
	{
		$this->assertEquals(12, $this->_object->getCode());
		$expected = 25;
		$this->_object->setCode($expected);
		$this->assertEquals($expected, $this->_object->getCode());
	}
	
	public function testGetMessage() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getMessage());
		$expected = 'qsdfghjklm';
		$this->_object->setMessage($expected);
		$this->assertEquals($expected, $this->_object->getMessage());
	}
	
	public function testGetResult() : void
	{
		$this->assertEquals(12, $this->_object->getResult());
		$expected = 25;
		$this->_object->setResult($expected);
		$this->assertEquals($expected, $this->_object->getResult());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrGouvEnsapLoginResponse('azertyuiop', 'azertyuiop', 12, 'azertyuiop', 12);
	}
	
}
