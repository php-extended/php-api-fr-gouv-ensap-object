<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-ensap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvEnsap\Test;

use PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapListeService;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrGouvEnsapListeServiceTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapListeService
 * @internal
 * @small
 */
class ApiFrGouvEnsapListeServiceTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrGouvEnsapListeService
	 */
	protected ApiFrGouvEnsapListeService $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testHasCompteindividuelretraite() : void
	{
		$this->assertFalse($this->_object->hasCompteindividuelretraite());
		$expected = true;
		$this->_object->setCompteindividuelretraite($expected);
		$this->assertTrue($this->_object->hasCompteindividuelretraite());
	}
	
	public function testHasDeclamep() : void
	{
		$this->assertFalse($this->_object->hasDeclamep());
		$expected = true;
		$this->_object->setDeclamep($expected);
		$this->assertTrue($this->_object->hasDeclamep());
	}
	
	public function testHasDemandedepartretraite() : void
	{
		$this->assertFalse($this->_object->hasDemandedepartretraite());
		$expected = true;
		$this->_object->setDemandedepartretraite($expected);
		$this->assertTrue($this->_object->hasDemandedepartretraite());
	}
	
	public function testHasDemandedepartretraiteprogressive() : void
	{
		$this->assertFalse($this->_object->hasDemandedepartretraiteprogressive());
		$expected = true;
		$this->_object->setDemandedepartretraiteprogressive($expected);
		$this->assertTrue($this->_object->hasDemandedepartretraiteprogressive());
	}
	
	public function testHasDocumentemployeur() : void
	{
		$this->assertFalse($this->_object->hasDocumentemployeur());
		$expected = true;
		$this->_object->setDocumentemployeur($expected);
		$this->assertTrue($this->_object->hasDocumentemployeur());
	}
	
	public function testHasMesap() : void
	{
		$this->assertFalse($this->_object->hasMesap());
		$expected = true;
		$this->_object->setMesap($expected);
		$this->assertTrue($this->_object->hasMesap());
	}
	
	public function testHasPension() : void
	{
		$this->assertFalse($this->_object->hasPension());
		$expected = true;
		$this->_object->setPension($expected);
		$this->assertTrue($this->_object->hasPension());
	}
	
	public function testHasRemuneration() : void
	{
		$this->assertFalse($this->_object->hasRemuneration());
		$expected = true;
		$this->_object->setRemuneration($expected);
		$this->assertTrue($this->_object->hasRemuneration());
	}
	
	public function testHasRemunerationpension() : void
	{
		$this->assertFalse($this->_object->hasRemunerationpension());
		$expected = true;
		$this->_object->setRemunerationpension($expected);
		$this->assertTrue($this->_object->hasRemunerationpension());
	}
	
	public function testHasRetraite() : void
	{
		$this->assertFalse($this->_object->hasRetraite());
		$expected = true;
		$this->_object->setRetraite($expected);
		$this->assertTrue($this->_object->hasRetraite());
	}
	
	public function testHasSimulation() : void
	{
		$this->assertFalse($this->_object->hasSimulation());
		$expected = true;
		$this->_object->setSimulation($expected);
		$this->assertTrue($this->_object->hasSimulation());
	}
	
	public function testHasSuividepartretraite() : void
	{
		$this->assertFalse($this->_object->hasSuividepartretraite());
		$expected = true;
		$this->_object->setSuividepartretraite($expected);
		$this->assertTrue($this->_object->hasSuividepartretraite());
	}
	
	public function testHasTitrepension() : void
	{
		$this->assertFalse($this->_object->hasTitrepension());
		$expected = true;
		$this->_object->setTitrepension($expected);
		$this->assertTrue($this->_object->hasTitrepension());
	}
	
	public function testHasSuividepartretraiteprogressive() : void
	{
		$this->assertFalse($this->_object->hasSuividepartretraiteprogressive());
		$expected = true;
		$this->_object->setSuividepartretraiteprogressive($expected);
		$this->assertTrue($this->_object->hasSuividepartretraiteprogressive());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrGouvEnsapListeService(false, false, false, false, false, false, false, false, false, false, false, false, false, false);
	}
	
}
