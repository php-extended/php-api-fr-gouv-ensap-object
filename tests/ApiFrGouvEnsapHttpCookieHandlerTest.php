<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-ensap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapHttpCookieHandler;
use PhpExtended\HttpMessage\Request;
use PhpExtended\HttpMessage\Response;
use PHPUnit\Framework\TestCase;

/**
 * EnsapHttpCookieHandlerTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapHttpCookieHandler
 *
 * @internal
 *
 * @small
 */
class ApiFrGouvEnsapHttpCookieHandlerTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrGouvEnsapHttpCookieHandler
	 */
	protected ApiFrGouvEnsapHttpCookieHandler $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testHandleRequestStandard() : void
	{
		$request = $this->_object->handleRequest(new Request());
		$this->assertEquals('foo=foobar; qux=quxbar', $request->getHeaderLine('Cookie'));
	}
	
	public function testHandleRequestXsrf() : void
	{
		$response = new Response();
		$response = $response->withAddedHeader('Set-Cookie', 'XSRF-TOKEN=foobarxsrf; path=/');
		$this->_object->handleResponse($response);
		$request = $this->_object->handleRequest(new Request());
		$this->assertEquals('foobarxsrf', $request->getHeaderLine('X-XSRF-TOKEN'));
	}
	
	public function testHandleResponseLemonap() : void
	{
		$response = new Response();
		$response = $response->withAddedHeader('Set-Cookie', 'lemonap=lemonade; path=/');
		$this->_object->handleResponse($response);
		$this->assertEquals('foo=foobar; qux=quxbar; lemonap=lemonade', $this->_object->getCookieStr());
	}
	
	public function testHandleResponseXsrf() : void
	{
		$response = new Response();
		$response = $response->withAddedHeader('Set-Cookie', 'XSRF-TOKEN=foobarxsrf; path=/');
		$this->_object->handleResponse($response);
		$this->assertEquals('foo=foobar; qux=quxbar; XSRF-TOKEN=foobarxsrf', $this->_object->getCookieStr());
	}
	
	public function testGetCookieStr() : void
	{
		$this->assertEquals('foo=foobar; qux=quxbar', $this->_object->getCookieStr());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrGouvEnsapHttpCookieHandler([
			'foo' => 'foobar',
			'qux' => 'quxbar',
		]);
	}
	
}
