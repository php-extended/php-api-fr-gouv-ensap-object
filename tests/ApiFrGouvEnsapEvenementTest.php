<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-ensap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvEnsap\Test;

use DateTimeImmutable;
use PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapEvenement;
use PhpExtended\Uuid\UuidParser;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrGouvEnsapEvenementTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapEvenement
 * @internal
 * @small
 */
class ApiFrGouvEnsapEvenementTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrGouvEnsapEvenement
	 */
	protected ApiFrGouvEnsapEvenement $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetEvenementId() : void
	{
		$this->assertEquals(12, $this->_object->getEvenementId());
		$expected = 25;
		$this->_object->setEvenementId($expected);
		$this->assertEquals($expected, $this->_object->getEvenementId());
	}
	
	public function testGetDateEvenement() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getDateEvenement());
		$expected = 'qsdfghjklm';
		$this->_object->setDateEvenement($expected);
		$this->assertEquals($expected, $this->_object->getDateEvenement());
	}
	
	public function testGetDocumentUuid() : void
	{
		$this->assertEquals((new UuidParser())->parse('9e2cfea1-afc9-4ecb-8a90-d477a5a90cfa'), $this->_object->getDocumentUuid());
		$expected = (new UuidParser())->parse('b592ccab-7e43-4de9-8460-34592395addb');
		$this->_object->setDocumentUuid($expected);
		$this->assertEquals($expected, $this->_object->getDocumentUuid());
	}
	
	public function testGetLibelle1() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getLibelle1());
		$expected = 'qsdfghjklm';
		$this->_object->setLibelle1($expected);
		$this->assertEquals($expected, $this->_object->getLibelle1());
	}
	
	public function testGetLibelle2() : void
	{
		$this->assertNull($this->_object->getLibelle2());
		$expected = 'qsdfghjklm';
		$this->_object->setLibelle2($expected);
		$this->assertEquals($expected, $this->_object->getLibelle2());
	}
	
	public function testGetLibelle3() : void
	{
		$this->assertNull($this->_object->getLibelle3());
		$expected = 'qsdfghjklm';
		$this->_object->setLibelle3($expected);
		$this->assertEquals($expected, $this->_object->getLibelle3());
	}
	
	public function testGetStatut() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getStatut());
		$expected = 'qsdfghjklm';
		$this->_object->setStatut($expected);
		$this->assertEquals($expected, $this->_object->getStatut());
	}
	
	public function testGetDateLecture() : void
	{
		$this->assertNull($this->_object->getDateLecture());
		$expected = DateTimeImmutable::createFromFormat('!Y-m-d H:i:s', '2001-01-01 00:00:01');
		$this->_object->setDateLecture($expected);
		$this->assertEquals($expected, $this->_object->getDateLecture());
	}
	
	public function testGetLibelleIcone() : void
	{
		$this->assertNull($this->_object->getLibelleIcone());
		$expected = 'qsdfghjklm';
		$this->_object->setLibelleIcone($expected);
		$this->assertEquals($expected, $this->_object->getLibelleIcone());
	}
	
	public function testGetIcone() : void
	{
		$this->assertNull($this->_object->getIcone());
		$expected = 'qsdfghjklm';
		$this->_object->setIcone($expected);
		$this->assertEquals($expected, $this->_object->getIcone());
	}
	
	public function testGetTri() : void
	{
		$this->assertNull($this->_object->getTri());
		$expected = 25;
		$this->_object->setTri($expected);
		$this->assertEquals($expected, $this->_object->getTri());
	}
	
	public function testGetActionIhm() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getActionIhm());
		$expected = 'qsdfghjklm';
		$this->_object->setActionIhm($expected);
		$this->assertEquals($expected, $this->_object->getActionIhm());
	}
	
	public function testGetService() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getService());
		$expected = 'qsdfghjklm';
		$this->_object->setService($expected);
		$this->assertEquals($expected, $this->_object->getService());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrGouvEnsapEvenement(12, 'azertyuiop', (new UuidParser())->parse('9e2cfea1-afc9-4ecb-8a90-d477a5a90cfa'), 'azertyuiop', 'azertyuiop', 'azertyuiop', 'azertyuiop');
	}
	
}
