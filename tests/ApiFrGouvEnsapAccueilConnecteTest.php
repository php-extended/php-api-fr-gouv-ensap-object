<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-ensap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvEnsap\Test;

use PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapAccueilConnecte;
use PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapAccueilDonnee;
use PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapContenu;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrGouvEnsapAccueilConnecteTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapAccueilConnecte
 * @internal
 * @small
 */
class ApiFrGouvEnsapAccueilConnecteTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrGouvEnsapAccueilConnecte
	 */
	protected ApiFrGouvEnsapAccueilConnecte $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetMessage() : void
	{
		$this->assertEquals(['key' => 'azertyuiop'], $this->_object->getMessage());
		$expected = ['key1' => 'qsdfghjklm', 'key2' => 'qsdfghjklm'];
		$this->_object->setMessage($expected);
		$this->assertEquals($expected, $this->_object->getMessage());
	}
	
	public function testGetMessagealerte() : void
	{
		$this->assertEquals(['key' => 'azertyuiop'], $this->_object->getMessagealerte());
		$expected = ['key1' => 'qsdfghjklm', 'key2' => 'qsdfghjklm'];
		$this->_object->setMessagealerte($expected);
		$this->assertEquals($expected, $this->_object->getMessagealerte());
	}
	
	public function testGetContenu() : void
	{
		$this->assertEquals($this->getMockBuilder(ApiFrGouvEnsapContenu::class)->disableOriginalConstructor()->getMock(), $this->_object->getContenu());
		$expected = $this->getMockBuilder(ApiFrGouvEnsapContenu::class)->disableOriginalConstructor()->getMock();
		$this->_object->setContenu($expected);
		$this->assertEquals($expected, $this->_object->getContenu());
	}
	
	public function testGetDonnee() : void
	{
		$this->assertEquals($this->getMockBuilder(ApiFrGouvEnsapAccueilDonnee::class)->disableOriginalConstructor()->getMock(), $this->_object->getDonnee());
		$expected = $this->getMockBuilder(ApiFrGouvEnsapAccueilDonnee::class)->disableOriginalConstructor()->getMock();
		$this->_object->setDonnee($expected);
		$this->assertEquals($expected, $this->_object->getDonnee());
	}
	
	public function testGetParametrage() : void
	{
		$this->assertEquals(['key' => 'azertyuiop'], $this->_object->getParametrage());
		$expected = ['key1' => 'qsdfghjklm', 'key2' => 'qsdfghjklm'];
		$this->_object->setParametrage($expected);
		$this->assertEquals($expected, $this->_object->getParametrage());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrGouvEnsapAccueilConnecte(['key' => 'azertyuiop'], ['key' => 'azertyuiop'], $this->getMockBuilder(ApiFrGouvEnsapContenu::class)->disableOriginalConstructor()->getMock(), $this->getMockBuilder(ApiFrGouvEnsapAccueilDonnee::class)->disableOriginalConstructor()->getMock(), ['key' => 'azertyuiop']);
	}
	
}
