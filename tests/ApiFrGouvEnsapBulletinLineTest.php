<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-ensap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvEnsap\Test;

use PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapBulletinLine;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrGouvEnsapBulletinLineTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapBulletinLine
 * @internal
 * @small
 */
class ApiFrGouvEnsapBulletinLineTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrGouvEnsapBulletinLine
	 */
	protected ApiFrGouvEnsapBulletinLine $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetCode() : void
	{
		$this->assertNull($this->_object->getCode());
		$expected = 25;
		$this->_object->setCode($expected);
		$this->assertEquals($expected, $this->_object->getCode());
	}
	
	public function testGetLibelle() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getLibelle());
		$expected = 'qsdfghjklm';
		$this->_object->setLibelle($expected);
		$this->assertEquals($expected, $this->_object->getLibelle());
	}
	
	public function testGetAPayer() : void
	{
		$this->assertNull($this->_object->getAPayer());
		$expected = 25;
		$this->_object->setAPayer($expected);
		$this->assertEquals($expected, $this->_object->getAPayer());
	}
	
	public function testGetADeduire() : void
	{
		$this->assertNull($this->_object->getADeduire());
		$expected = 25;
		$this->_object->setADeduire($expected);
		$this->assertEquals($expected, $this->_object->getADeduire());
	}
	
	public function testGetPourInfo() : void
	{
		$this->assertNull($this->_object->getPourInfo());
		$expected = 25;
		$this->_object->setPourInfo($expected);
		$this->assertEquals($expected, $this->_object->getPourInfo());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrGouvEnsapBulletinLine('azertyuiop');
	}
	
}
