<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-ensap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvEnsap\Test;

use PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapRawDocument;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrGouvEnsapRawDocumentTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapRawDocument
 * @internal
 * @small
 */
class ApiFrGouvEnsapRawDocumentTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrGouvEnsapRawDocument
	 */
	protected ApiFrGouvEnsapRawDocument $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetStatusCode() : void
	{
		$this->assertEquals(12, $this->_object->getStatusCode());
		$expected = 25;
		$this->_object->setStatusCode($expected);
		$this->assertEquals($expected, $this->_object->getStatusCode());
	}
	
	public function testGetFileName() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getFileName());
		$expected = 'qsdfghjklm';
		$this->_object->setFileName($expected);
		$this->assertEquals($expected, $this->_object->getFileName());
	}
	
	public function testGetLength() : void
	{
		$this->assertEquals(12, $this->_object->getLength());
		$expected = 25;
		$this->_object->setLength($expected);
		$this->assertEquals($expected, $this->_object->getLength());
	}
	
	public function testGetMimeType() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getMimeType());
		$expected = 'qsdfghjklm';
		$this->_object->setMimeType($expected);
		$this->assertEquals($expected, $this->_object->getMimeType());
	}
	
	public function testGetRawData() : void
	{
		$this->assertEquals('azertyuiop', $this->_object->getRawData());
		$expected = 'qsdfghjklm';
		$this->_object->setRawData($expected);
		$this->assertEquals($expected, $this->_object->getRawData());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrGouvEnsapRawDocument(12, 'azertyuiop', 12, 'azertyuiop', 'azertyuiop');
	}
	
}
