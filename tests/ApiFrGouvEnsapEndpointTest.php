<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-ensap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

use PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapAccueilConnecte;
use PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapBulletin;
use PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapDocument;
use PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapEndpoint;
use PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapEndpointInterface;
use PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapHabilitation;
use PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapProfile;
use PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapRawDocument;
use PhpExtended\HttpMessage\Response;
use PhpExtended\HttpMessage\StringStream;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * ApiFrGouvEnsapEndpointTest test file.
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapEndpoint
 *
 * @internal
 *
 * @small
 */
class ApiFrGouvEnsapEndpointTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrGouvEnsapEndpoint
	 */
	protected ApiFrGouvEnsapEndpoint $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testInitialiserHabilitations() : void
	{
		if(\is_file(__DIR__.'/../credentials.cache'))
		{
			$credentials = require __DIR__.'/../credentials.cache';
			$this->_object->login($credentials['username'], $credentials['password']);
		}
		else
		{
			$this->markTestSkipped('No credentials provided.');
			
			return;
		}
		
		$habilitations = $this->_object->initialiserHabilitations();
		
		$this->assertInstanceOf(ApiFrGouvEnsapHabilitation::class, $habilitations);
	}
	
	public function testGetDataAccueilConnecte() : void
	{
		if(\is_file(__DIR__.'/../credentials.cache'))
		{
			$credentials = require __DIR__.'/../credentials.cache';
			$this->_object->login($credentials['username'], $credentials['password']);
		}
		else
		{
			$this->markTestSkipped('No credentials provided.');
			
			return;
		}
		
		$dataAccueilConnecte = $this->_object->getDataAccueilConnecte();
		
		$this->assertInstanceOf(ApiFrGouvEnsapAccueilConnecte::class, $dataAccueilConnecte);
	}
	
	public function testGetDataRemuneration() : void
	{
		if(\is_file(__DIR__.'/../credentials.cache'))
		{
			$credentials = require __DIR__.'/../credentials.cache';
			$this->_object->login($credentials['username'], $credentials['password']);
		}
		else
		{
			$this->markTestSkipped('No credentials provided.');
			
			return;
		}
		
		$dataRemuneration = $this->_object->getDataRemuneration(((int) \date('Y')) - 1);
		
		foreach($dataRemuneration as $remuneration)
		{
			$this->assertInstanceOf(ApiFrGouvEnsapDocument::class, $remuneration);
		}
	}
	
	public function testGetDataProfil() : void
	{
		if(\is_file(__DIR__.'/../credentials.cache'))
		{
			$credentials = require __DIR__.'/../credentials.cache';
			$this->_object->login($credentials['username'], $credentials['password']);
		}
		else
		{
			$this->markTestSkipped('No credentials provided.');
			
			return;
		}
		
		$this->assertInstanceOf(ApiFrGouvEnsapProfile::class, $this->_object->getDataProfil());
	}
	
	public function testGetRawDocument() : void
	{
		if(\is_file(__DIR__.'/../credentials.cache'))
		{
			$credentials = require __DIR__.'/../credentials.cache';
			$this->_object->login($credentials['username'], $credentials['password']);
		}
		else
		{
			$this->markTestSkipped('No credentials provided.');
			
			return;
		}
		
		$dataRemuneration = $this->_object->getDataRemuneration(((int) \date('Y')));
		
		/** @var ApiFrGouvEnsapDocument $remuneration */
		foreach($dataRemuneration as $remuneration)
		{
			$rawDocument = $this->_object->getRawDocument($remuneration->getDocumentUuid());
			
			$this->assertInstanceOf(ApiFrGouvEnsapRawDocument::class, $rawDocument);
			
			break;
		}
	}
	
	public function testGetParsedBulletin() : void
	{
		if(\is_file(__DIR__.'/../credentials.cache'))
		{
			$credentials = require __DIR__.'/../credentials.cache';
			$this->_object->login($credentials['username'], $credentials['password']);
		}
		else
		{
			$this->markTestSkipped('No credentials provided.');
			
			return;
		}
		
		$dataRemuneration = $this->_object->getDataRemuneration(((int) \date('Y')) - 1);
		
		/** @var ApiFrGouvEnsapDocument $document */
		foreach($dataRemuneration as $document)
		{
			if($document->getCodeSousThemeGed() !== ApiFrGouvEnsapEndpointInterface::CODE_SOUS_THEME_GED_BULLETIN)
			{
				continue;
			}
			
			try
			{
				$bulletin = $this->_object->getBulletinFromDocument($document);
			}
			catch(Throwable $exc)
			{
				@\file_put_contents('/tmp/'.$document->getNomDocument(), $this->_object->getRawDocument($document->getDocumentUuid())->getRawData());
				
				throw new RuntimeException('Failed to parse document '.$document->nomDocument.', written document at /tmp/'.$document->nomDocument, -1, $exc);
			}
			
			$this->assertInstanceOf(ApiFrGouvEnsapBulletin::class, $bulletin);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$client = new class() implements ClientInterface
		{
			public function sendRequest(RequestInterface $request) : ResponseInterface
			{
				$headers = [];
				
				foreach($request->getHeaders() as $headerKey => $headerArr)
				{
					$headers[] = $headerKey.': '.\implode(', ', $headerArr);
				}
				// \var_dump($headers);
				$context = \stream_context_create(['http' => [
					'method' => $request->getMethod(),
					'header' => \implode("\r\n", $headers)."\r\n",
					'content' => $request->getBody()->__toString(),
					'follow_location' => 0,
					'ignore_errors' => true,
				]]);
				
				$http_response_header = [];
				
				$data = (string) \file_get_contents($request->getUri()->__toString(), false, $context);
				
				$response = new Response();
				// \var_dump($http_response_header);
				
				foreach($http_response_header as $header)
				{
					$matches = [];
					if(\preg_match('#([\\w-]+):(.*)$#', $header, $matches))
					{
						$response = $response->withAddedHeader($matches[1], $matches[2]);
					}
				}
				
				return $response->withBody(new StringStream($data));
			}
			
		};
		
		$this->_object = new ApiFrGouvEnsapEndpoint($client);
	}
	
}
