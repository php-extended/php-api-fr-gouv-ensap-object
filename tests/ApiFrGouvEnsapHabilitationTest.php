<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-ensap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvEnsap\Test;

use PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapHabilitation;
use PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapIdentification;
use PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapListeService;
use PHPUnit\Framework\TestCase;

/**
 * ApiFrGouvEnsapHabilitationTest test file.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74TestMetadata
 * 
 * @author Anastaszor
 * @covers \PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapHabilitation
 * @internal
 * @small
 */
class ApiFrGouvEnsapHabilitationTest extends TestCase
{
	
	/**
	 * The object to test.
	 * 
	 * @var ApiFrGouvEnsapHabilitation
	 */
	protected ApiFrGouvEnsapHabilitation $_object;
	
	public function testToString() : void
	{
		$this->assertEquals(\get_class($this->_object).'@'.\spl_object_hash($this->_object), $this->_object->__toString());
	}
	
	public function testGetIdentification() : void
	{
		$this->assertEquals($this->getMockBuilder(ApiFrGouvEnsapIdentification::class)->disableOriginalConstructor()->getMock(), $this->_object->getIdentification());
		$expected = $this->getMockBuilder(ApiFrGouvEnsapIdentification::class)->disableOriginalConstructor()->getMock();
		$this->_object->setIdentification($expected);
		$this->assertEquals($expected, $this->_object->getIdentification());
	}
	
	public function testHasLectureSeule() : void
	{
		$this->assertFalse($this->_object->hasLectureSeule());
		$expected = true;
		$this->_object->setLectureSeule($expected);
		$this->assertTrue($this->_object->hasLectureSeule());
	}
	
	public function testGetListeAnneeRemuneration() : void
	{
		$this->assertEquals([12], $this->_object->getListeAnneeRemuneration());
		$expected = [25, 25];
		$this->_object->setListeAnneeRemuneration($expected);
		$this->assertEquals($expected, $this->_object->getListeAnneeRemuneration());
	}
	
	public function testGetListeService() : void
	{
		$this->assertEquals($this->getMockBuilder(ApiFrGouvEnsapListeService::class)->disableOriginalConstructor()->getMock(), $this->_object->getListeService());
		$expected = $this->getMockBuilder(ApiFrGouvEnsapListeService::class)->disableOriginalConstructor()->getMock();
		$this->_object->setListeService($expected);
		$this->assertEquals($expected, $this->_object->getListeService());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PHPUnit\Framework\TestCase::setUp()
	 */
	protected function setUp() : void
	{
		$this->_object = new ApiFrGouvEnsapHabilitation($this->getMockBuilder(ApiFrGouvEnsapIdentification::class)->disableOriginalConstructor()->getMock(), false, [12], $this->getMockBuilder(ApiFrGouvEnsapListeService::class)->disableOriginalConstructor()->getMock());
	}
	
}
