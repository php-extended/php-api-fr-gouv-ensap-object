<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-ensap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvEnsap;

use DateTimeImmutable;
use DateTimeInterface;
use DateTimeZone;
use Exception;
use InvalidArgumentException;
use LogicException;
use OutOfRangeException;
use PhpExtended\Pdf2json\Pdf2jsonDocumentInterface;
use PhpExtended\Pdf2json\Pdf2jsonExtractor;
use PhpExtended\Pdf2json\Pdf2jsonExtractorInterface;
use PhpExtended\Pdf2json\Pdf2jsonText;
use PhpExtended\Pdf2json\Pdf2jsonTextInterface;
use RuntimeException;

/**
 * ApiFrGouvEnsapPdfParser class file.
 * 
 * The pdf parser that transforms raw pdf data into php structures.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 */
class ApiFrGouvEnsapPdfParser implements ApiFrGouvEnsapPdfParserInterface
{
	
	public const HEIGHT_ONE_LINE = 14;
	
	/**
	 * The pdf2json extractor.
	 * 
	 * @var ?Pdf2jsonExtractorInterface
	 */
	protected ?Pdf2jsonExtractorInterface $_extractor = null;
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapPdfParserInterface::parseBulletinFromFile()
	 * @throws InvalidArgumentException
	 * @throws LogicException
	 * @throws OutOfRangeException
	 * @throws RuntimeException
	 */
	public function parseBulletinFromFile(string $filePath) : ApiFrGouvEnsapBulletinInterface
	{
		return $this->buildBulletin($this->getExtractor()->extractFromPdfFile($filePath, true, false));
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapPdfParserInterface::parseBulletinFromPdfString()
	 * @throws InvalidArgumentException
	 * @throws LogicException
	 * @throws OutOfRangeException
	 * @throws RuntimeException
	 */
	public function parseBulletinFromPdfString(string $pdfBinaryString) : ApiFrGouvEnsapBulletinInterface
	{
		return $this->buildBulletin($this->getExtractor()->extractFromPdfString($pdfBinaryString));
	}
	
	/**
	 * Gets the pdf2json extractor.
	 * 
	 * @return Pdf2jsonExtractorInterface
	 */
	protected function getExtractor() : Pdf2jsonExtractorInterface
	{
		if(null === $this->_extractor)
		{
			$this->_extractor = new Pdf2jsonExtractor();
		}
		
		return $this->_extractor;
	}
	
	/**
	 * Builds an ensap bulletin from the given document.
	 * 
	 * @param Pdf2jsonDocumentInterface $document
	 * @return ApiFrGouvEnsapBulletinInterface
	 * @throws InvalidArgumentException if the data does not match
	 * @throws OutOfRangeException if there is no data in the bbox
	 * @throws RuntimeException if cannot be converted to date
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 * @SuppressWarnings("PHPMD.ExcessiveMethodLength")
	 */
	protected function buildBulletin(Pdf2jsonDocumentInterface $document) : ApiFrGouvEnsapBulletinInterface
	{
		$bulletin = new ApiFrGouvEnsapBulletin();
		$bulletin->setEmitter($this->getTextBbox($document, 10, 120, 3 * self::HEIGHT_ONE_LINE, 140));
		$bulletin->setMonthLetter($this->getRegexBbox($document, 33, 470, self::HEIGHT_ONE_LINE, 100, '#^[A-Z]+\\s\\d{4}$#'));
		$bulletin->setNumOrder($this->getTextBbox($document, 18, 780, self::HEIGHT_ONE_LINE, 80));
		
		try
		{
			$bulletin->setWorkTime($this->getRegexBbox($document, 45, 770, self::HEIGHT_ONE_LINE, 90, '#^(\\d+,|\\+\\sDE\\s)\\d+\\sH$#'));
		}
		catch(OutOfRangeException $exc)
		{
			// ignore
		}
		
		$bulletin->setPoste1($this->getTextBbox($document, 91, 115, 2 * self::HEIGHT_ONE_LINE, 85));
		$bulletin->setPoste2($this->getTextBbox($document, 91, 225, 2 * self::HEIGHT_ONE_LINE, 85));
		$bulletin->setLibellePoste($this->getTextBbox($document, 91, 318, 2 * self::HEIGHT_ONE_LINE, 320));
		
		try
		{
			$bulletin->setSiretEmployeur($this->getRegexBbox($document, 91, 740, self::HEIGHT_ONE_LINE, 130, '#^\\d{14}$#'));
		}
		catch(OutOfRangeException $exc)
		{
			// retry, one line lower, if fail => exc
			$bulletin->setSiretEmployeur($this->getRegexBbox($document, 106, 740, self::HEIGHT_ONE_LINE, 130, '#^\\d{14}$#'));
		}
		
		$bulletin->setIdMin((int) $this->getRegexBbox($document, 160, 21, self::HEIGHT_ONE_LINE, 30, '#^\\d+$#'));
		$bulletin->setIdNir($this->getRegexBbox($document, 160, 60, self::HEIGHT_ONE_LINE, 200, '#^\\d\\s\\d{2}\\s\\d{2}\\s\\d{2}\\s\\d{3}\\s\\d{3}\\s\\d{2}$#'));
		$bulletin->setIdNodos((int) $this->getRegexBbox($document, 160, 285, self::HEIGHT_ONE_LINE, 15, '#^\\d{1,2}$#'));
		$bulletin->setGrade($this->getTextBbox($document, 160, 320, self::HEIGHT_ONE_LINE, 180));
		$bulletin->setChildCharged((int) $this->getTextBbox($document, 160, 520, self::HEIGHT_ONE_LINE, 20));
		$bulletin->setEchelon($this->getTextBbox($document, 160, 565, self::HEIGHT_ONE_LINE, 24));
		
		try
		{
			$bulletin->setIndiceOrNbHours((int) $this->getRegexBbox($document, 160, 620, self::HEIGHT_ONE_LINE, 50, '#^\\d+$#'));
		}
		catch(OutOfRangeException $exc)
		{
			// ignore
		}
		
		try
		{
			$bulletin->setTauxHoraire((int) $this->getRegexBbox($document, 160, 690, self::HEIGHT_ONE_LINE, 64, '#^\\d+$#'));
		}
		catch(OutOfRangeException $exc)
		{
			// ignore
		}
		
		try
		{
			$bulletin->setTpsPartiel($this->getTextBbox($document, 160, 780, self::HEIGHT_ONE_LINE, 80));
		}
		catch(OutOfRangeException $exc)
		{
			// ignore
		}
		
		$positionLines = [];
		
		/** @var Pdf2jsonTextInterface $text */
		foreach($document->getText() as $text)
		{
			if('€' === $text->getData() && 204 < $text->getTop() && 862 > $text->getTop())
			{
				$positionLines[] = $text->getTop() - 2; // offset the top (the euro symbol is smaller)
			}
		}
		
		$lines = [];
		
		foreach($positionLines as $h)
		{
			$line = $this->parseLine($document, $h);
			if(null !== $line)
			{
				$lines[] = $line;
			}
		}
		
		$bulletin->setLines($lines);
		$bulletin->setTotalEmployeur($this->getAmount($document, 944, 328, self::HEIGHT_ONE_LINE, 80));
		$bulletin->setTotalAPayer($this->getAmount($document, 944, 575, self::HEIGHT_ONE_LINE, 80));
		$bulletin->setTotalADeduire($this->getAmount($document, 944, 676, self::HEIGHT_ONE_LINE, 80));
		
		try
		{
			$bulletin->setTotalPourInfo($this->getAmount($document, 944, 780, self::HEIGHT_ONE_LINE, 80));
		}
		catch(OutOfRangeException $exc)
		{
			// ignore
		}
		
		$bulletin->setTotalNetAPayer($this->getAmount($document, 972, 620, self::HEIGHT_ONE_LINE, 120));
		
		try
		{
			$bulletin->setBaseSsYear($this->getAmount($document, 1018, 40, self::HEIGHT_ONE_LINE, 110));
		}
		catch(OutOfRangeException $exc)
		{
			// ignore
		}
		
		try
		{
			$bulletin->setBaseSsMonth($this->getAmount($document, 1018, 176, self::HEIGHT_ONE_LINE, 110));
		}
		catch(OutOfRangeException $exc)
		{
			// ignore
		}
		
		$bulletin->setMontantImposableYear($this->getAmount($document, 1070, 40, self::HEIGHT_ONE_LINE, 110));
		$bulletin->setMontahtImposableMonth($this->getAmount($document, 1070, 176, self::HEIGHT_ONE_LINE, 110));
		$bulletin->setComptable($this->getTextBbox($document, 1125, 30, self::HEIGHT_ONE_LINE, 250));
		$bulletin->setDatePaid($this->getDate($document, 1180, 30, self::HEIGHT_ONE_LINE, 250));
		$bulletin->setAccountPaidIban($this->getRegexBbox($document, 1228, 14, self::HEIGHT_ONE_LINE, 300, '#^([A-Z0-9]{4}\\s){6}[A-Z0-9]{3}$#'));
		$bulletin->setAccountPaidBic($this->getRegexBbox($document, 1246, 14, self::HEIGHT_ONE_LINE, 300, '#^[A-Z0-9]{11}$#'));
		
		return $bulletin;
	}
	
	/**
	 * Gets the line under the top position.
	 * 
	 * @param Pdf2jsonDocumentInterface $document
	 * @param integer $top
	 * @return ?ApiFrGouvEnsapBulletinLineInterface
	 * @throws InvalidArgumentException
	 * @throws RuntimeException
	 */
	protected function parseLine(Pdf2jsonDocumentInterface $document, int $top) : ?ApiFrGouvEnsapBulletinLineInterface
	{
		try
		{
			$libelle = $this->getTextBbox($document, $top, 78, self::HEIGHT_ONE_LINE, 460);
		}
		catch(OutOfRangeException $exc)
		{
			return null; // nothing to see at this height
		}
		
		$bulletinLine = new ApiFrGouvEnsapBulletinLine($libelle);
		
		try
		{
			$bulletinLine->setCode((int) $this->getRegexBbox($document, $top, 25, self::HEIGHT_ONE_LINE, 45, '#^\\d{6}$#'));
		}
		catch(OutOfRangeException $exc)
		{
			// ignore
		}
		
		try
		{
			$bulletinLine->setAPayer($this->getAmount($document, $top, 550, self::HEIGHT_ONE_LINE, 110));
		}
		catch(OutOfRangeException $exc)
		{
			// ignore
		}
		
		try
		{
			$bulletinLine->setADeduire($this->getAmount($document, $top, 660, self::HEIGHT_ONE_LINE, 110));
		}
		catch(OutOfRangeException $exc)
		{
			// ignore
		}
		
		try
		{
			$bulletinLine->setPourInfo($this->getAmount($document, $top, 770, self::HEIGHT_ONE_LINE, 110));
		}
		catch(OutOfRangeException $exc)
		{
			// ignore
		}
		
		if(null !== $bulletinLine->getCode())
		{
			if(null === $bulletinLine->getAPayer() && null === $bulletinLine->getADeduire() && null === $bulletinLine->getPourInfo())
			{
				throw new RuntimeException('Failed to get accurate line information for line '.((string) \json_encode($bulletinLine)));
			}
		}
		
		return $bulletinLine;
	}
	
	/**
	 * Gets the date represented by the value present in the bbox.
	 * 
	 * @param Pdf2jsonDocumentInterface $document
	 * @param int $top
	 * @param int $left
	 * @param int $height
	 * @param int $width
	 * @return DateTimeInterface
	 * @throws InvalidArgumentException if the data does not match
	 * @throws OutOfRangeException if there is no data in the bbox
	 * @throws RuntimeException if cannot be converted to date
	 */
	protected function getDate(Pdf2jsonDocumentInterface $document, int $top, int $left, int $height, int $width) : DateTimeInterface
	{
		return $this->pdfDate($this->getRegexBbox($document, $top, $left, $height, $width, '#^\\d+\\s[A-Z]+\\s\\d{4}$#'));
	}
	
	/**
	 * Gets the amount represented by the value present in the bbox.
	 * 
	 * @param Pdf2jsonDocumentInterface $document
	 * @param integer $top
	 * @param integer $left
	 * @param integer $height
	 * @param integer $width
	 * @return integer the amount (euro cents)
	 * @throws InvalidArgumentException if the data does not match
	 * @throws OutOfRangeException if there is no data in the bbox
	 */
	protected function getAmount(Pdf2jsonDocumentInterface $document, int $top, int $left, int $height, int $width) : int
	{
		try
		{
			return (int) \str_replace([',', ' '], '', $this->getRegexBbox($document, $top, $left, $height, $width, '#^-?(\\d+\\s)?\\d+,\\d{2}$#'));
		}
		catch(InvalidArgumentException $exc)
		{
			$message = 'Failed to get amount expected in bbox [{x}, {y}, {xh}, {yw}]';
			$context = ['{x}' => $top, '{y}' => $left, '{xh}' => $top + $height, '{yw}' => $left + $width];
			
			throw new InvalidArgumentException(\strtr($message, $context), -1, $exc);
		}
	}
	
	/**
	 * Gets the text in the bbox, (top, left, top+height, left+width) matching
	 * the regex. Throws an exception if the returned text does not match.
	 * 
	 * @param Pdf2jsonDocumentInterface $document
	 * @param integer $top
	 * @param integer $left
	 * @param integer $height
	 * @param integer $width
	 * @param string $pattern
	 * @return string
	 * @throws InvalidArgumentException if the data does not match
	 * @throws OutOfRangeException if there is no data in the bbox
	 */
	protected function getRegexBbox(Pdf2jsonDocumentInterface $document, int $top, int $left, int $height, int $width, string $pattern) : string
	{
		$value = $this->getTextBbox($document, $top, $left, $height, $width);
		
		/** @psalm-suppress ArgumentTypeCoercion */
		if(!\preg_match($pattern, $value))
		{
			$message = 'Failed to get string matching {regex} : "{value}" in bbox [{x}, {y}, {xh}, {yw}]';
			$context = ['{regex}' => $pattern, '{value}' => $value, '{x}' => $top, '{y}' => $left, '{xh}' => $top + $height, '{yw}' => $left + $width];
			
			throw new InvalidArgumentException(\strtr($message, $context));
		}
		
		return $value;
	}
	
	/**
	 * Gets the text in the bbox (top, left, top+height, left+width).
	 * 
	 * @param Pdf2jsonDocumentInterface $document
	 * @param integer $top
	 * @param integer $left
	 * @param integer $height
	 * @param integer $width
	 * @return string
	 * @throws OutOfRangeException if there is no data in the selected bbox
	 * @SuppressWarnings("PHPMD.NPathComplexity")
	 * @SuppressWarnings("PHPMD.CyclomaticComplexity")
	 */
	protected function getTextBbox(Pdf2jsonDocumentInterface $document, int $top, int $left, int $height, int $width) : string
	{
		$selected = [];
		
		/** @var Pdf2jsonText $text */
		foreach($document->getText() as $text)
		{
			if($text->getTop() >= $top && $top + $height >= $text->getTop() && $text->getLeft() >= $left && $left + $width >= $text->getLeft())
			{
				$selected[] = $text;
			}
		}
		
		if(empty($selected))
		{
			$message = 'No string found in bbox [{x}, {y}, {xh}, {yw}]';
			$context = ['{x}' => $top, '{y}' => $left, '{xh}' => $top + $height, '{yw}' => $left + $width];
			
			throw new OutOfRangeException(\strtr($message, $context));
		}
		
		\usort($selected, function(Pdf2jsonText $first, Pdf2jsonText $second) : int
		{
			if((int) $first->getTop() - (int) $second->getTop() > self::HEIGHT_ONE_LINE)
			{
				return 1;
			}
			if((int) $second->getTop() - (int) $first->getTop() > self::HEIGHT_ONE_LINE)
			{
				return -1;
			}
			
			if((int) $first->getLeft() > (int) $second->getLeft())
			{
				return 1;
			}
			
			if((int) $second->getLeft() > (int) $first->getLeft())
			{
				return -1;
			}
			
			return 0;
		});
		
		$parts = \array_map(function(Pdf2jsonText $text) : string
		{
			return (string) $text->getData();
		}, $selected);
		
		$imploded = \trim(\implode(' ', $parts));
		
		while(\strpos($imploded, '  ') !== false)
		{
			$imploded = \str_replace('  ', ' ', $imploded);
		}
		
		if(empty($imploded))
		{
			$message = 'No string found in bbox [{x}, {y}, {xh}, {yw}]';
			$context = ['{x}' => $top, '{y}' => $left, '{xh}' => $top + $height, '{yw}' => $left + $width];
			
			throw new OutOfRangeException(\strtr($message, $context));
		}
		
		return $imploded;
	}
	
	/**
	 * Parses the given string (fr) as datetime.
	 * 
	 * @param string $string
	 * @return DateTimeInterface
	 * @throws RuntimeException
	 */
	protected function pdfDate(string $string) : DateTimeInterface
	{
		$str = \strtr($string, [
			'JANVIER' => 'Jan',
			'FEVRIER' => 'Feb',
			'MARS' => 'Mar',
			'AVRIL' => 'Apr',
			'MAI' => 'May',
			'JUIN' => 'Jun',
			'JUILLET' => 'Jul',
			'AOUT' => 'Aug',
			'SEPTEMBRE' => 'Sep',
			'OCTOBRE' => 'Oct',
			'NOVEMBRE' => 'Nov',
			'DECEMBRE' => 'Dec',
		]);
		
		$dti = DateTimeImmutable::createFromFormat('!j M Y', $str, new DateTimeZone('Europe/Paris'));
		if(false === $dti)
		{
			$message = 'Failed to parse date from string "{str}"';
			$context = ['{str}' => $str];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		
		return $dti;
	}
	
}
