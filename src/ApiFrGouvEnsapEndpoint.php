<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-ensap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvEnsap;

use Exception;
use InvalidArgumentException;
use Iterator;
use LogicException;
use OutOfRangeException;
use PhpExtended\DataProvider\JsonStringDataProvider;
use PhpExtended\DataProvider\UnprovidableThrowable;
use PhpExtended\HttpMessage\RequestFactory;
use PhpExtended\HttpMessage\StreamFactory;
use PhpExtended\HttpMessage\UriFactory;
use PhpExtended\Parser\ParseThrowable;
use PhpExtended\Reifier\ReificationThrowable;
use PhpExtended\Reifier\Reifier;
use PhpExtended\Reifier\ReifierInterface;
use PhpExtended\Uuid\UuidInterface;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;
use Psr\Http\Message\UriFactoryInterface;
use RuntimeException;

/**
 * ApiFrGouvEnsapEndpoint class file.
 * 
 * This class takes all entering requests and give back all the api responses,
 * decoded into their objet form.
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.CouplingBetweenObjects")
 */
class ApiFrGouvEnsapEndpoint implements ApiFrGouvEnsapEndpointInterface
{
	
	public const HOST = 'https://ensap.gouv.fr/';
	
	/**
	 * The code for success that is returned in the json objects.
	 *
	 * @var integer
	 */
	public const JSON_CODE_SUCCESS = 60;
	
	/**
	 * The http client.
	 *
	 * @var ClientInterface
	 */
	protected ClientInterface $_httpClient;
	
	/**
	 * The uri factory.
	 *
	 * @var UriFactoryInterface
	 */
	protected UriFactoryInterface $_uriFactory;
	
	/**
	 * The request factory.
	 *
	 * @var RequestFactoryInterface
	 */
	protected RequestFactoryInterface $_requestFactory;
	
	/**
	 * The stream factory.
	 *
	 * @var StreamFactoryInterface
	 */
	protected StreamFactoryInterface $_streamFactory;
	
	/**
	 * The reifier.
	 *
	 * @var ReifierInterface
	 */
	protected ReifierInterface $_reifier;
	
	/**
	 * The cookie handler.
	 * 
	 * @var ApiFrGouvEnsapHttpCookieHandler
	 */
	protected ApiFrGouvEnsapHttpCookieHandler $_cookieHandler;
	
	/**
	 * Constructor of the instance. This object acts as a coordinator between
	 * all the modules of the mioga application.
	 *
	 * This method builds the object but does not connect to it, so it never
	 * throws exceptions.
	 *
	 * @param ClientInterface $client
	 * @param ?UriFactoryInterface $uriFactory
	 * @param ?RequestFactoryInterface $requestFactory
	 * @param ?StreamFactoryInterface $streamFactory
	 * @param ?ReifierInterface $reifier
	 */
	public function __construct(
		ClientInterface $client,
		?UriFactoryInterface $uriFactory = null,
		?RequestFactoryInterface $requestFactory = null,
		?StreamFactoryInterface $streamFactory = null,
		?ReifierInterface $reifier = null
	) {
		$this->_httpClient = $client;
		$this->_uriFactory = $uriFactory ?? new UriFactory();
		$this->_requestFactory = $requestFactory ?? new RequestFactory();
		$this->_streamFactory = $streamFactory ?? new StreamFactory();
		$this->_reifier = $reifier ?? new Reifier();
		
		$configuration = $this->_reifier->getConfiguration();
		
		$configuration->setIterableInnerType(ApiFrGouvEnsapAccueilConnecte::class, 'message', 'string');
		$configuration->setIterableInnerType(ApiFrGouvEnsapAccueilConnecte::class, 'messagealerte', 'string');
		$configuration->setIterableInnerType(ApiFrGouvEnsapAccueilConnecte::class, 'parametrage', 'string');
		$configuration->setIterableInnerType(ApiFrGouvEnsapAccueilDonnee::class, 'listeEvenement', ApiFrGouvEnsapEvenement::class);
		$configuration->setIterableInnerType(ApiFrGouvEnsapAccueilDonnee::class, 'listeAnneeRemuneration', 'int');
		$configuration->setIterableInnerType(ApiFrGouvEnsapContenu::class, 'champ', 'string');
		$configuration->setIterableInnerType(ApiFrGouvEnsapContenu::class, 'paragraphe', ApiFrGouvEnsapText::class);
		$configuration->setIterableInnerType(ApiFrGouvEnsapHabilitation::class, 'listeAnneeRemuneration', 'int');
		$configuration->setIterableInnerType(ApiFrGouvEnsapProfile::class, 'message', 'string');
		$configuration->setIterableInnerType(ApiFrGouvEnsapProfile::class, 'messagealerte', 'string');
		$configuration->setIterableInnerType(ApiFrGouvEnsapProfile::class, 'parametrage', 'string');
		
		$this->_cookieHandler = new ApiFrGouvEnsapHttpCookieHandler();
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapEndpointInterface::login()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ParseThrowable
	 * @throws ReificationThrowable
	 * @throws RuntimeException
	 * @throws UnprovidableThrowable
	 */
	public function login(string $username, string $password) : bool
	{
		$query = \http_build_query(['identifiant' => $username, 'secret' => $password]);
		$uri = $this->_uriFactory->createUri(self::HOST);
		$request = $this->_requestFactory->createRequest('POST', $uri);
		$request = $request->withAddedHeader('Accept', 'application/json');
		$request = $request->withAddedHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
		$request = $request->withAddedHeader('Content-Length', (string) \mb_strlen($query));
		$request = $request->withAddedHeader('X-Php-Follow-Location', '0');
		$request = $request->withBody($this->_streamFactory->createStream($query));
		$request = $this->_cookieHandler->handleRequest($request);
		$response = $this->_httpClient->sendRequest($request);
		$response = $this->_cookieHandler->handleResponse($response);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		/** @var ApiFrGouvEnsapLoginResponse $login */
		$login = $this->_reifier->reify(ApiFrGouvEnsapLoginResponse::class, $json->provideOne());
		
		if(ApiFrGouvEnsapEndpoint::JSON_CODE_SUCCESS !== $login->getCode())
		{
			$message = 'Failed to log in with given username "{user}" and password : {c} {m}';
			$context = ['{user}' => $username, '{c}' => $login->getCode(), '{m}' => $login->getMessage()];
			
			throw new RuntimeException(\strtr($message, $context));
		}
		
		return true;
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapEndpointInterface::initialiserHabilitations()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function initialiserHabilitations() : ApiFrGouvEnsapHabilitationInterface
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'prive/initialiserhabilitation/v1');
		$request = $this->_requestFactory->createRequest('POST', $uri);
		$request = $request->withAddedHeader('Accept', 'application/json');
		$request = $request->withAddedHeader('Content-Type', 'application/json; charset=UTF-8');
		$request = $request->withAddedHeader('Content-Length', '2');
		$request = $request->withBody($this->_streamFactory->createStream('{}'));
		$request = $this->_cookieHandler->handleRequest($request);
		$response = $this->_httpClient->sendRequest($request);
		$response = $this->_cookieHandler->handleResponse($response);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reify(ApiFrGouvEnsapHabilitation::class, $json->provideOne());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapEndpointInterface::getDataAccueilConnecte()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getDataAccueilConnecte() : ApiFrGouvEnsapAccueilConnecteInterface
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'prive/accueilconnecte/v1');
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$request = $request->withAddedHeader('Accept', 'application/json');
		$request = $this->_cookieHandler->handleRequest($request);
		$response = $this->_httpClient->sendRequest($request);
		$response = $this->_cookieHandler->handleResponse($response);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reify(ApiFrGouvEnsapAccueilConnecte::class, $json->provideOne());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapEndpointInterface::getDataRemuneration()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getDataRemuneration(int $year) : Iterator
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'prive/remunerationpaie/v1/?annee='.((string) $year));
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$request = $request->withAddedHeader('Accept', 'application/json');
		$request = $this->_cookieHandler->handleRequest($request);
		$response = $this->_httpClient->sendRequest($request);
		$response = $this->_cookieHandler->handleResponse($response);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reifyIterator(ApiFrGouvEnsapDocument::class, $json->provideIterator());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapEndpointInterface::getDataProfil()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ReificationThrowable
	 * @throws UnprovidableThrowable
	 */
	public function getDataProfil() : ApiFrGouvEnsapProfileInterface
	{
		$uri = $this->_uriFactory->createUri(self::HOST.'prive/profil/v1');
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$request = $request->withAddedHeader('Accept', 'application/json');
		$request = $this->_cookieHandler->handleRequest($request);
		$response = $this->_httpClient->sendRequest($request);
		$response = $this->_cookieHandler->handleResponse($response);
		$json = new JsonStringDataProvider($response->getBody()->__toString());
		
		return $this->_reifier->reify(ApiFrGouvEnsapProfile::class, $json->provideOne());
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapEndpointInterface::getRawDocument()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ParseThrowable
	 */
	public function getRawDocument(UuidInterface $documentUuid) : ApiFrGouvEnsapRawDocumentInterface
	{
		$url = self::HOST.'prive/telechargerremunerationpaie/v1?documentUuid='.$documentUuid->__toString();
		$uri = $this->_uriFactory->createUri($url);
		$request = $this->_requestFactory->createRequest('GET', $uri);
		$request = $request->withAddedHeader('Accept', '*/*');
		$request = $this->_cookieHandler->handleRequest($request);
		$response = $this->_httpClient->sendRequest($request);
		$response = $this->_cookieHandler->handleResponse($response);
		
		return new ApiFrGouvEnsapRawDocument(
			$response->getStatusCode(),
			\trim(\str_replace('attachment; filename=', '', $response->getHeaderLine('Content-Disposition')), '"'),
			(int) $response->getHeaderLine('Content-Length'),
			$response->getHeaderLine('Content-Type'),
			$response->getBody()->__toString(),
		);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapEndpointInterface::getBulletinFromDocument()
	 * @throws ClientExceptionInterface
	 * @throws InvalidArgumentException should not happen
	 * @throws ParseThrowable
	 * @throws RuntimeException
	 */
	public function getBulletinFromDocument(ApiFrGouvEnsapDocumentInterface $document) : ApiFrGouvEnsapBulletinInterface
	{
		if($document->getCodeSousThemeGed() !== ApiFrGouvEnsapEndpointInterface::CODE_SOUS_THEME_GED_BULLETIN)
		{
			throw new RuntimeException('The given document is not a bulletin.');
		}
		
		$rawDocument = $this->getRawDocument($document->getDocumentUuid());
		
		return $this->getBulletinFromRawDocument($rawDocument);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapEndpointInterface::getBulletinFromRawDocument()
	 */
	public function getBulletinFromRawDocument(ApiFrGouvEnsapRawDocumentInterface $document) : ApiFrGouvEnsapBulletinInterface
	{
		try
		{
			return $this->getBulletinFromBinaryString((string) $document->getRawData());
		}
		catch(Exception $exc)
		{
			$message = 'Failed to parse bulletin from document {name} ({bytes})';
			$context = ['{name}' => $document->getFileName(), '{bytes}' => $document->getLength()];
			
			throw new RuntimeException(\strtr($message, $context), -1, $exc);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapEndpointInterface::getBulletinFromBinaryString()
	 * @throws InvalidArgumentException
	 * @throws LogicException
	 * @throws OutOfRangeException
	 * @throws RuntimeException
	 */
	public function getBulletinFromBinaryString(string $pdfBinaryString) : ApiFrGouvEnsapBulletinInterface
	{
		$parser = new ApiFrGouvEnsapPdfParser();
		
		return $parser->parseBulletinFromPdfString($pdfBinaryString);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapEndpointInterface::getBulletinFromFilePath()
	 * @throws LogicException
	 * @throws OutOfRangeException
	 * @throws RuntimeException
	 */
	public function getBulletinFromFilePath(string $pdfFilePath) : ApiFrGouvEnsapBulletinInterface
	{
		$parser = new ApiFrGouvEnsapPdfParser();
		
		return $parser->parseBulletinFromFile($pdfFilePath);
	}
	
}
