<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-ensap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvEnsap;

/**
 * ApiFrGouvEnsapHabilitation class file.
 * 
 * This is a simple implementation of the ApiFrGouvEnsapHabilitationInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ApiFrGouvEnsapHabilitation implements ApiFrGouvEnsapHabilitationInterface
{
	
	/**
	 * The identity of the user.
	 * 
	 * @var ApiFrGouvEnsapIdentificationInterface
	 */
	protected ApiFrGouvEnsapIdentificationInterface $_identification;
	
	/**
	 * Whether this habilitation is read only.
	 * 
	 * @var bool
	 */
	protected bool $_lectureSeule;
	
	/**
	 * The annees where the remuneration is available.
	 * 
	 * @var array<int, int>
	 */
	protected array $_listeAnneeRemuneration = [];
	
	/**
	 * The list of services this habilitation gives access.
	 * 
	 * @var ApiFrGouvEnsapListeServiceInterface
	 */
	protected ApiFrGouvEnsapListeServiceInterface $_listeService;
	
	/**
	 * Constructor for ApiFrGouvEnsapHabilitation with private members.
	 * 
	 * @param ApiFrGouvEnsapIdentificationInterface $identification
	 * @param bool $lectureSeule
	 * @param array<int, int> $listeAnneeRemuneration
	 * @param ApiFrGouvEnsapListeServiceInterface $listeService
	 */
	public function __construct(ApiFrGouvEnsapIdentificationInterface $identification, bool $lectureSeule, array $listeAnneeRemuneration, ApiFrGouvEnsapListeServiceInterface $listeService)
	{
		$this->setIdentification($identification);
		$this->setLectureSeule($lectureSeule);
		$this->setListeAnneeRemuneration($listeAnneeRemuneration);
		$this->setListeService($listeService);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the identity of the user.
	 * 
	 * @param ApiFrGouvEnsapIdentificationInterface $identification
	 * @return ApiFrGouvEnsapHabilitationInterface
	 */
	public function setIdentification(ApiFrGouvEnsapIdentificationInterface $identification) : ApiFrGouvEnsapHabilitationInterface
	{
		$this->_identification = $identification;
		
		return $this;
	}
	
	/**
	 * Gets the identity of the user.
	 * 
	 * @return ApiFrGouvEnsapIdentificationInterface
	 */
	public function getIdentification() : ApiFrGouvEnsapIdentificationInterface
	{
		return $this->_identification;
	}
	
	/**
	 * Sets whether this habilitation is read only.
	 * 
	 * @param bool $lectureSeule
	 * @return ApiFrGouvEnsapHabilitationInterface
	 */
	public function setLectureSeule(bool $lectureSeule) : ApiFrGouvEnsapHabilitationInterface
	{
		$this->_lectureSeule = $lectureSeule;
		
		return $this;
	}
	
	/**
	 * Gets whether this habilitation is read only.
	 * 
	 * @return bool
	 */
	public function hasLectureSeule() : bool
	{
		return $this->_lectureSeule;
	}
	
	/**
	 * Sets the annees where the remuneration is available.
	 * 
	 * @param array<int, int> $listeAnneeRemuneration
	 * @return ApiFrGouvEnsapHabilitationInterface
	 */
	public function setListeAnneeRemuneration(array $listeAnneeRemuneration) : ApiFrGouvEnsapHabilitationInterface
	{
		$this->_listeAnneeRemuneration = $listeAnneeRemuneration;
		
		return $this;
	}
	
	/**
	 * Gets the annees where the remuneration is available.
	 * 
	 * @return array<int, int>
	 */
	public function getListeAnneeRemuneration() : array
	{
		return $this->_listeAnneeRemuneration;
	}
	
	/**
	 * Sets the list of services this habilitation gives access.
	 * 
	 * @param ApiFrGouvEnsapListeServiceInterface $listeService
	 * @return ApiFrGouvEnsapHabilitationInterface
	 */
	public function setListeService(ApiFrGouvEnsapListeServiceInterface $listeService) : ApiFrGouvEnsapHabilitationInterface
	{
		$this->_listeService = $listeService;
		
		return $this;
	}
	
	/**
	 * Gets the list of services this habilitation gives access.
	 * 
	 * @return ApiFrGouvEnsapListeServiceInterface
	 */
	public function getListeService() : ApiFrGouvEnsapListeServiceInterface
	{
		return $this->_listeService;
	}
	
}
