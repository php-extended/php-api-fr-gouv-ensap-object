<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-ensap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvEnsap;

/**
 * ApiFrGouvEnsapAccueilConnecte class file.
 * 
 * This is a simple implementation of the
 * ApiFrGouvEnsapAccueilConnecteInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ApiFrGouvEnsapAccueilConnecte implements ApiFrGouvEnsapAccueilConnecteInterface
{
	
	/**
	 * The message list used by the UI.
	 * 
	 * @var array<string, string>
	 */
	protected array $_message = [];
	
	/**
	 * The message list used as alerts.
	 * 
	 * @var array<string, string>
	 */
	protected array $_messagealerte = [];
	
	/**
	 * The contents of the accueil data.
	 * 
	 * @var ApiFrGouvEnsapContenuInterface
	 */
	protected ApiFrGouvEnsapContenuInterface $_contenu;
	
	/**
	 * The public data of the user.
	 * 
	 * @var ApiFrGouvEnsapAccueilDonneeInterface
	 */
	protected ApiFrGouvEnsapAccueilDonneeInterface $_donnee;
	
	/**
	 * All the parameters.
	 * 
	 * @var array<string, string>
	 */
	protected array $_parametrage = [];
	
	/**
	 * Constructor for ApiFrGouvEnsapAccueilConnecte with private members.
	 * 
	 * @param array<string, string> $message
	 * @param array<string, string> $messagealerte
	 * @param ApiFrGouvEnsapContenuInterface $contenu
	 * @param ApiFrGouvEnsapAccueilDonneeInterface $donnee
	 * @param array<string, string> $parametrage
	 */
	public function __construct(array $message, array $messagealerte, ApiFrGouvEnsapContenuInterface $contenu, ApiFrGouvEnsapAccueilDonneeInterface $donnee, array $parametrage)
	{
		$this->setMessage($message);
		$this->setMessagealerte($messagealerte);
		$this->setContenu($contenu);
		$this->setDonnee($donnee);
		$this->setParametrage($parametrage);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the message list used by the UI.
	 * 
	 * @param array<string, string> $message
	 * @return ApiFrGouvEnsapAccueilConnecteInterface
	 */
	public function setMessage(array $message) : ApiFrGouvEnsapAccueilConnecteInterface
	{
		$this->_message = $message;
		
		return $this;
	}
	
	/**
	 * Gets the message list used by the UI.
	 * 
	 * @return array<string, string>
	 */
	public function getMessage() : array
	{
		return $this->_message;
	}
	
	/**
	 * Sets the message list used as alerts.
	 * 
	 * @param array<string, string> $messagealerte
	 * @return ApiFrGouvEnsapAccueilConnecteInterface
	 */
	public function setMessagealerte(array $messagealerte) : ApiFrGouvEnsapAccueilConnecteInterface
	{
		$this->_messagealerte = $messagealerte;
		
		return $this;
	}
	
	/**
	 * Gets the message list used as alerts.
	 * 
	 * @return array<string, string>
	 */
	public function getMessagealerte() : array
	{
		return $this->_messagealerte;
	}
	
	/**
	 * Sets the contents of the accueil data.
	 * 
	 * @param ApiFrGouvEnsapContenuInterface $contenu
	 * @return ApiFrGouvEnsapAccueilConnecteInterface
	 */
	public function setContenu(ApiFrGouvEnsapContenuInterface $contenu) : ApiFrGouvEnsapAccueilConnecteInterface
	{
		$this->_contenu = $contenu;
		
		return $this;
	}
	
	/**
	 * Gets the contents of the accueil data.
	 * 
	 * @return ApiFrGouvEnsapContenuInterface
	 */
	public function getContenu() : ApiFrGouvEnsapContenuInterface
	{
		return $this->_contenu;
	}
	
	/**
	 * Sets the public data of the user.
	 * 
	 * @param ApiFrGouvEnsapAccueilDonneeInterface $donnee
	 * @return ApiFrGouvEnsapAccueilConnecteInterface
	 */
	public function setDonnee(ApiFrGouvEnsapAccueilDonneeInterface $donnee) : ApiFrGouvEnsapAccueilConnecteInterface
	{
		$this->_donnee = $donnee;
		
		return $this;
	}
	
	/**
	 * Gets the public data of the user.
	 * 
	 * @return ApiFrGouvEnsapAccueilDonneeInterface
	 */
	public function getDonnee() : ApiFrGouvEnsapAccueilDonneeInterface
	{
		return $this->_donnee;
	}
	
	/**
	 * Sets all the parameters.
	 * 
	 * @param array<string, string> $parametrage
	 * @return ApiFrGouvEnsapAccueilConnecteInterface
	 */
	public function setParametrage(array $parametrage) : ApiFrGouvEnsapAccueilConnecteInterface
	{
		$this->_parametrage = $parametrage;
		
		return $this;
	}
	
	/**
	 * Gets all the parameters.
	 * 
	 * @return array<string, string>
	 */
	public function getParametrage() : array
	{
		return $this->_parametrage;
	}
	
}
