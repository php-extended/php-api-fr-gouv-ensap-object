<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-ensap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvEnsap;

/**
 * ApiFrGouvEnsapRawDocument class file.
 * 
 * This is a simple implementation of the ApiFrGouvEnsapRawDocumentInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ApiFrGouvEnsapRawDocument implements ApiFrGouvEnsapRawDocumentInterface
{
	
	/**
	 * The status code of the http request.
	 * 
	 * @var int
	 */
	protected int $_statusCode;
	
	/**
	 * The file name of the document.
	 * 
	 * @var string
	 */
	protected string $_fileName;
	
	/**
	 * The length of the document.
	 * 
	 * @var int
	 */
	protected int $_length;
	
	/**
	 * The mime type of the document.
	 * 
	 * @var string
	 */
	protected string $_mimeType;
	
	/**
	 * The raw data of the document.
	 * 
	 * @var string
	 */
	protected string $_rawData;
	
	/**
	 * Constructor for ApiFrGouvEnsapRawDocument with private members.
	 * 
	 * @param int $statusCode
	 * @param string $fileName
	 * @param int $length
	 * @param string $mimeType
	 * @param string $rawData
	 */
	public function __construct(int $statusCode, string $fileName, int $length, string $mimeType, string $rawData)
	{
		$this->setStatusCode($statusCode);
		$this->setFileName($fileName);
		$this->setLength($length);
		$this->setMimeType($mimeType);
		$this->setRawData($rawData);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the status code of the http request.
	 * 
	 * @param int $statusCode
	 * @return ApiFrGouvEnsapRawDocumentInterface
	 */
	public function setStatusCode(int $statusCode) : ApiFrGouvEnsapRawDocumentInterface
	{
		$this->_statusCode = $statusCode;
		
		return $this;
	}
	
	/**
	 * Gets the status code of the http request.
	 * 
	 * @return int
	 */
	public function getStatusCode() : int
	{
		return $this->_statusCode;
	}
	
	/**
	 * Sets the file name of the document.
	 * 
	 * @param string $fileName
	 * @return ApiFrGouvEnsapRawDocumentInterface
	 */
	public function setFileName(string $fileName) : ApiFrGouvEnsapRawDocumentInterface
	{
		$this->_fileName = $fileName;
		
		return $this;
	}
	
	/**
	 * Gets the file name of the document.
	 * 
	 * @return string
	 */
	public function getFileName() : string
	{
		return $this->_fileName;
	}
	
	/**
	 * Sets the length of the document.
	 * 
	 * @param int $length
	 * @return ApiFrGouvEnsapRawDocumentInterface
	 */
	public function setLength(int $length) : ApiFrGouvEnsapRawDocumentInterface
	{
		$this->_length = $length;
		
		return $this;
	}
	
	/**
	 * Gets the length of the document.
	 * 
	 * @return int
	 */
	public function getLength() : int
	{
		return $this->_length;
	}
	
	/**
	 * Sets the mime type of the document.
	 * 
	 * @param string $mimeType
	 * @return ApiFrGouvEnsapRawDocumentInterface
	 */
	public function setMimeType(string $mimeType) : ApiFrGouvEnsapRawDocumentInterface
	{
		$this->_mimeType = $mimeType;
		
		return $this;
	}
	
	/**
	 * Gets the mime type of the document.
	 * 
	 * @return string
	 */
	public function getMimeType() : string
	{
		return $this->_mimeType;
	}
	
	/**
	 * Sets the raw data of the document.
	 * 
	 * @param string $rawData
	 * @return ApiFrGouvEnsapRawDocumentInterface
	 */
	public function setRawData(string $rawData) : ApiFrGouvEnsapRawDocumentInterface
	{
		$this->_rawData = $rawData;
		
		return $this;
	}
	
	/**
	 * Gets the raw data of the document.
	 * 
	 * @return string
	 */
	public function getRawData() : string
	{
		return $this->_rawData;
	}
	
}
