<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-ensap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvEnsap;

/**
 * ApiFrGouvEnsapListeService class file.
 * 
 * This is a simple implementation of the ApiFrGouvEnsapListeServiceInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ApiFrGouvEnsapListeService implements ApiFrGouvEnsapListeServiceInterface
{
	
	/**
	 * Whether the service compte individuel retraite is available.
	 * 
	 * @var bool
	 */
	protected bool $_compteindividuelretraite;
	
	/**
	 * Whether the service declaration maladie professionnelle? is available.
	 * 
	 * @var bool
	 */
	protected bool $_declamep;
	
	/**
	 * Whether the service demande depart retraite is available.
	 * 
	 * @var bool
	 */
	protected bool $_demandedepartretraite;
	
	/**
	 * Whether the service demande départ retraite progressive is available.
	 * 
	 * @var bool
	 */
	protected bool $_demandedepartretraiteprogressive;
	
	/**
	 * Whether the service document employeur is available.
	 * 
	 * @var bool
	 */
	protected bool $_documentemployeur;
	
	/**
	 * Whether the service mesap is available.
	 * 
	 * @var bool
	 */
	protected bool $_mesap;
	
	/**
	 * Whether the service pension is available.
	 * 
	 * @var bool
	 */
	protected bool $_pension;
	
	/**
	 * Whether the service remuneration is available.
	 * 
	 * @var bool
	 */
	protected bool $_remuneration;
	
	/**
	 * Whether the service remuneration pension is available.
	 * 
	 * @var bool
	 */
	protected bool $_remunerationpension;
	
	/**
	 * Whether the service retraite is available.
	 * 
	 * @var bool
	 */
	protected bool $_retraite;
	
	/**
	 * Whether the service simulation is available.
	 * 
	 * @var bool
	 */
	protected bool $_simulation;
	
	/**
	 * Whether the service suivi depart retraite is available.
	 * 
	 * @var bool
	 */
	protected bool $_suividepartretraite;
	
	/**
	 * Whether the service titre pension is available.
	 * 
	 * @var bool
	 */
	protected bool $_titrepension;
	
	/**
	 * Whether the service suivi départ retraite progressive is available.
	 * 
	 * @var bool
	 */
	protected bool $_suividepartretraiteprogressive;
	
	/**
	 * Constructor for ApiFrGouvEnsapListeService with private members.
	 * 
	 * @param bool $compteindividuelretraite
	 * @param bool $declamep
	 * @param bool $demandedepartretraite
	 * @param bool $demandedepartretraiteprogressive
	 * @param bool $documentemployeur
	 * @param bool $mesap
	 * @param bool $pension
	 * @param bool $remuneration
	 * @param bool $remunerationpension
	 * @param bool $retraite
	 * @param bool $simulation
	 * @param bool $suividepartretraite
	 * @param bool $titrepension
	 * @param bool $suividepartretraiteprogressive
	 * @SuppressWarnings("PHPMD.ExcessiveParameterList")
	 */
	public function __construct(bool $compteindividuelretraite, bool $declamep, bool $demandedepartretraite, bool $demandedepartretraiteprogressive, bool $documentemployeur, bool $mesap, bool $pension, bool $remuneration, bool $remunerationpension, bool $retraite, bool $simulation, bool $suividepartretraite, bool $titrepension, bool $suividepartretraiteprogressive)
	{
		$this->setCompteindividuelretraite($compteindividuelretraite);
		$this->setDeclamep($declamep);
		$this->setDemandedepartretraite($demandedepartretraite);
		$this->setDemandedepartretraiteprogressive($demandedepartretraiteprogressive);
		$this->setDocumentemployeur($documentemployeur);
		$this->setMesap($mesap);
		$this->setPension($pension);
		$this->setRemuneration($remuneration);
		$this->setRemunerationpension($remunerationpension);
		$this->setRetraite($retraite);
		$this->setSimulation($simulation);
		$this->setSuividepartretraite($suividepartretraite);
		$this->setTitrepension($titrepension);
		$this->setSuividepartretraiteprogressive($suividepartretraiteprogressive);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets whether the service compte individuel retraite is available.
	 * 
	 * @param bool $compteindividuelretraite
	 * @return ApiFrGouvEnsapListeServiceInterface
	 */
	public function setCompteindividuelretraite(bool $compteindividuelretraite) : ApiFrGouvEnsapListeServiceInterface
	{
		$this->_compteindividuelretraite = $compteindividuelretraite;
		
		return $this;
	}
	
	/**
	 * Gets whether the service compte individuel retraite is available.
	 * 
	 * @return bool
	 */
	public function hasCompteindividuelretraite() : bool
	{
		return $this->_compteindividuelretraite;
	}
	
	/**
	 * Sets whether the service declaration maladie professionnelle? is
	 * available.
	 * 
	 * @param bool $declamep
	 * @return ApiFrGouvEnsapListeServiceInterface
	 */
	public function setDeclamep(bool $declamep) : ApiFrGouvEnsapListeServiceInterface
	{
		$this->_declamep = $declamep;
		
		return $this;
	}
	
	/**
	 * Gets whether the service declaration maladie professionnelle? is
	 * available.
	 * 
	 * @return bool
	 */
	public function hasDeclamep() : bool
	{
		return $this->_declamep;
	}
	
	/**
	 * Sets whether the service demande depart retraite is available.
	 * 
	 * @param bool $demandedepartretraite
	 * @return ApiFrGouvEnsapListeServiceInterface
	 */
	public function setDemandedepartretraite(bool $demandedepartretraite) : ApiFrGouvEnsapListeServiceInterface
	{
		$this->_demandedepartretraite = $demandedepartretraite;
		
		return $this;
	}
	
	/**
	 * Gets whether the service demande depart retraite is available.
	 * 
	 * @return bool
	 */
	public function hasDemandedepartretraite() : bool
	{
		return $this->_demandedepartretraite;
	}
	
	/**
	 * Sets whether the service demande départ retraite progressive is
	 * available.
	 * 
	 * @param bool $demandedepartretraiteprogressive
	 * @return ApiFrGouvEnsapListeServiceInterface
	 */
	public function setDemandedepartretraiteprogressive(bool $demandedepartretraiteprogressive) : ApiFrGouvEnsapListeServiceInterface
	{
		$this->_demandedepartretraiteprogressive = $demandedepartretraiteprogressive;
		
		return $this;
	}
	
	/**
	 * Gets whether the service demande départ retraite progressive is
	 * available.
	 * 
	 * @return bool
	 */
	public function hasDemandedepartretraiteprogressive() : bool
	{
		return $this->_demandedepartretraiteprogressive;
	}
	
	/**
	 * Sets whether the service document employeur is available.
	 * 
	 * @param bool $documentemployeur
	 * @return ApiFrGouvEnsapListeServiceInterface
	 */
	public function setDocumentemployeur(bool $documentemployeur) : ApiFrGouvEnsapListeServiceInterface
	{
		$this->_documentemployeur = $documentemployeur;
		
		return $this;
	}
	
	/**
	 * Gets whether the service document employeur is available.
	 * 
	 * @return bool
	 */
	public function hasDocumentemployeur() : bool
	{
		return $this->_documentemployeur;
	}
	
	/**
	 * Sets whether the service mesap is available.
	 * 
	 * @param bool $mesap
	 * @return ApiFrGouvEnsapListeServiceInterface
	 */
	public function setMesap(bool $mesap) : ApiFrGouvEnsapListeServiceInterface
	{
		$this->_mesap = $mesap;
		
		return $this;
	}
	
	/**
	 * Gets whether the service mesap is available.
	 * 
	 * @return bool
	 */
	public function hasMesap() : bool
	{
		return $this->_mesap;
	}
	
	/**
	 * Sets whether the service pension is available.
	 * 
	 * @param bool $pension
	 * @return ApiFrGouvEnsapListeServiceInterface
	 */
	public function setPension(bool $pension) : ApiFrGouvEnsapListeServiceInterface
	{
		$this->_pension = $pension;
		
		return $this;
	}
	
	/**
	 * Gets whether the service pension is available.
	 * 
	 * @return bool
	 */
	public function hasPension() : bool
	{
		return $this->_pension;
	}
	
	/**
	 * Sets whether the service remuneration is available.
	 * 
	 * @param bool $remuneration
	 * @return ApiFrGouvEnsapListeServiceInterface
	 */
	public function setRemuneration(bool $remuneration) : ApiFrGouvEnsapListeServiceInterface
	{
		$this->_remuneration = $remuneration;
		
		return $this;
	}
	
	/**
	 * Gets whether the service remuneration is available.
	 * 
	 * @return bool
	 */
	public function hasRemuneration() : bool
	{
		return $this->_remuneration;
	}
	
	/**
	 * Sets whether the service remuneration pension is available.
	 * 
	 * @param bool $remunerationpension
	 * @return ApiFrGouvEnsapListeServiceInterface
	 */
	public function setRemunerationpension(bool $remunerationpension) : ApiFrGouvEnsapListeServiceInterface
	{
		$this->_remunerationpension = $remunerationpension;
		
		return $this;
	}
	
	/**
	 * Gets whether the service remuneration pension is available.
	 * 
	 * @return bool
	 */
	public function hasRemunerationpension() : bool
	{
		return $this->_remunerationpension;
	}
	
	/**
	 * Sets whether the service retraite is available.
	 * 
	 * @param bool $retraite
	 * @return ApiFrGouvEnsapListeServiceInterface
	 */
	public function setRetraite(bool $retraite) : ApiFrGouvEnsapListeServiceInterface
	{
		$this->_retraite = $retraite;
		
		return $this;
	}
	
	/**
	 * Gets whether the service retraite is available.
	 * 
	 * @return bool
	 */
	public function hasRetraite() : bool
	{
		return $this->_retraite;
	}
	
	/**
	 * Sets whether the service simulation is available.
	 * 
	 * @param bool $simulation
	 * @return ApiFrGouvEnsapListeServiceInterface
	 */
	public function setSimulation(bool $simulation) : ApiFrGouvEnsapListeServiceInterface
	{
		$this->_simulation = $simulation;
		
		return $this;
	}
	
	/**
	 * Gets whether the service simulation is available.
	 * 
	 * @return bool
	 */
	public function hasSimulation() : bool
	{
		return $this->_simulation;
	}
	
	/**
	 * Sets whether the service suivi depart retraite is available.
	 * 
	 * @param bool $suividepartretraite
	 * @return ApiFrGouvEnsapListeServiceInterface
	 */
	public function setSuividepartretraite(bool $suividepartretraite) : ApiFrGouvEnsapListeServiceInterface
	{
		$this->_suividepartretraite = $suividepartretraite;
		
		return $this;
	}
	
	/**
	 * Gets whether the service suivi depart retraite is available.
	 * 
	 * @return bool
	 */
	public function hasSuividepartretraite() : bool
	{
		return $this->_suividepartretraite;
	}
	
	/**
	 * Sets whether the service titre pension is available.
	 * 
	 * @param bool $titrepension
	 * @return ApiFrGouvEnsapListeServiceInterface
	 */
	public function setTitrepension(bool $titrepension) : ApiFrGouvEnsapListeServiceInterface
	{
		$this->_titrepension = $titrepension;
		
		return $this;
	}
	
	/**
	 * Gets whether the service titre pension is available.
	 * 
	 * @return bool
	 */
	public function hasTitrepension() : bool
	{
		return $this->_titrepension;
	}
	
	/**
	 * Sets whether the service suivi départ retraite progressive is
	 * available.
	 * 
	 * @param bool $suividepartretraiteprogressive
	 * @return ApiFrGouvEnsapListeServiceInterface
	 */
	public function setSuividepartretraiteprogressive(bool $suividepartretraiteprogressive) : ApiFrGouvEnsapListeServiceInterface
	{
		$this->_suividepartretraiteprogressive = $suividepartretraiteprogressive;
		
		return $this;
	}
	
	/**
	 * Gets whether the service suivi départ retraite progressive is
	 * available.
	 * 
	 * @return bool
	 */
	public function hasSuividepartretraiteprogressive() : bool
	{
		return $this->_suividepartretraiteprogressive;
	}
	
}
