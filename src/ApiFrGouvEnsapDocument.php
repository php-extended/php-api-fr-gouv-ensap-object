<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-ensap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvEnsap;

use DateTimeInterface;
use PhpExtended\Uuid\UuidInterface;

/**
 * ApiFrGouvEnsapDocument class file.
 * 
 * This is a simple implementation of the ApiFrGouvEnsapDocumentInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ApiFrGouvEnsapDocument implements ApiFrGouvEnsapDocumentInterface
{
	
	/**
	 * The uuid of the document relative to this event.
	 * 
	 * @var UuidInterface
	 */
	protected UuidInterface $_documentUuid;
	
	/**
	 * The code of the subtheme from the GED where those docs are stored.
	 * 
	 * @var ?string
	 */
	protected ?string $_codeSousThemeGed = null;
	
	/**
	 * The first label of the document.
	 * 
	 * @var string
	 */
	protected string $_libelle1;
	
	/**
	 * The second label of the document.
	 * 
	 * @var ?string
	 */
	protected ?string $_libelle2 = null;
	
	/**
	 * The third label of the document.
	 * 
	 * @var ?string
	 */
	protected ?string $_libelle3 = null;
	
	/**
	 * The date when this document was made available.
	 * 
	 * @var DateTimeInterface
	 */
	protected DateTimeInterface $_dateDocument;
	
	/**
	 * The year of availability of the document.
	 * 
	 * @var int
	 */
	protected int $_annee;
	
	/**
	 * Constructor for ApiFrGouvEnsapDocument with private members.
	 * 
	 * @param UuidInterface $documentUuid
	 * @param string $libelle1
	 * @param DateTimeInterface $dateDocument
	 * @param int $annee
	 */
	public function __construct(UuidInterface $documentUuid, string $libelle1, DateTimeInterface $dateDocument, int $annee)
	{
		$this->setDocumentUuid($documentUuid);
		$this->setLibelle1($libelle1);
		$this->setDateDocument($dateDocument);
		$this->setAnnee($annee);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the uuid of the document relative to this event.
	 * 
	 * @param UuidInterface $documentUuid
	 * @return ApiFrGouvEnsapDocumentInterface
	 */
	public function setDocumentUuid(UuidInterface $documentUuid) : ApiFrGouvEnsapDocumentInterface
	{
		$this->_documentUuid = $documentUuid;
		
		return $this;
	}
	
	/**
	 * Gets the uuid of the document relative to this event.
	 * 
	 * @return UuidInterface
	 */
	public function getDocumentUuid() : UuidInterface
	{
		return $this->_documentUuid;
	}
	
	/**
	 * Sets the code of the subtheme from the GED where those docs are stored.
	 * 
	 * @param ?string $codeSousThemeGed
	 * @return ApiFrGouvEnsapDocumentInterface
	 */
	public function setCodeSousThemeGed(?string $codeSousThemeGed) : ApiFrGouvEnsapDocumentInterface
	{
		$this->_codeSousThemeGed = $codeSousThemeGed;
		
		return $this;
	}
	
	/**
	 * Gets the code of the subtheme from the GED where those docs are stored.
	 * 
	 * @return ?string
	 */
	public function getCodeSousThemeGed() : ?string
	{
		return $this->_codeSousThemeGed;
	}
	
	/**
	 * Sets the first label of the document.
	 * 
	 * @param string $libelle1
	 * @return ApiFrGouvEnsapDocumentInterface
	 */
	public function setLibelle1(string $libelle1) : ApiFrGouvEnsapDocumentInterface
	{
		$this->_libelle1 = $libelle1;
		
		return $this;
	}
	
	/**
	 * Gets the first label of the document.
	 * 
	 * @return string
	 */
	public function getLibelle1() : string
	{
		return $this->_libelle1;
	}
	
	/**
	 * Sets the second label of the document.
	 * 
	 * @param ?string $libelle2
	 * @return ApiFrGouvEnsapDocumentInterface
	 */
	public function setLibelle2(?string $libelle2) : ApiFrGouvEnsapDocumentInterface
	{
		$this->_libelle2 = $libelle2;
		
		return $this;
	}
	
	/**
	 * Gets the second label of the document.
	 * 
	 * @return ?string
	 */
	public function getLibelle2() : ?string
	{
		return $this->_libelle2;
	}
	
	/**
	 * Sets the third label of the document.
	 * 
	 * @param ?string $libelle3
	 * @return ApiFrGouvEnsapDocumentInterface
	 */
	public function setLibelle3(?string $libelle3) : ApiFrGouvEnsapDocumentInterface
	{
		$this->_libelle3 = $libelle3;
		
		return $this;
	}
	
	/**
	 * Gets the third label of the document.
	 * 
	 * @return ?string
	 */
	public function getLibelle3() : ?string
	{
		return $this->_libelle3;
	}
	
	/**
	 * Sets the date when this document was made available.
	 * 
	 * @param DateTimeInterface $dateDocument
	 * @return ApiFrGouvEnsapDocumentInterface
	 */
	public function setDateDocument(DateTimeInterface $dateDocument) : ApiFrGouvEnsapDocumentInterface
	{
		$this->_dateDocument = $dateDocument;
		
		return $this;
	}
	
	/**
	 * Gets the date when this document was made available.
	 * 
	 * @return DateTimeInterface
	 */
	public function getDateDocument() : DateTimeInterface
	{
		return $this->_dateDocument;
	}
	
	/**
	 * Sets the year of availability of the document.
	 * 
	 * @param int $annee
	 * @return ApiFrGouvEnsapDocumentInterface
	 */
	public function setAnnee(int $annee) : ApiFrGouvEnsapDocumentInterface
	{
		$this->_annee = $annee;
		
		return $this;
	}
	
	/**
	 * Gets the year of availability of the document.
	 * 
	 * @return int
	 */
	public function getAnnee() : int
	{
		return $this->_annee;
	}
	
}
