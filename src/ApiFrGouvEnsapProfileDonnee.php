<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-ensap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvEnsap;

use PhpExtended\Email\EmailAddressInterface;

/**
 * ApiFrGouvEnsapProfileDonnee class file.
 * 
 * This is a simple implementation of the ApiFrGouvEnsapProfileDonneeInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ApiFrGouvEnsapProfileDonnee implements ApiFrGouvEnsapProfileDonneeInterface
{
	
	/**
	 * Whether the current user has subscribed to notifications.
	 * 
	 * @var bool
	 */
	protected bool $_estAbonneNotification;
	
	/**
	 * Whether the current user has subscribed to ensap specific informations.
	 * 
	 * @var ?bool
	 */
	protected ?bool $_estAbonneMailEnsap = null;
	
	/**
	 * The email address used for notifications.
	 * 
	 * @var ?EmailAddressInterface
	 */
	protected ?EmailAddressInterface $_mailNotification = null;
	
	/**
	 * The email address used as principal.
	 * 
	 * @var EmailAddressInterface
	 */
	protected EmailAddressInterface $_mailPrincipal;
	
	/**
	 * The email address used as secondary.
	 * 
	 * @var EmailAddressInterface
	 */
	protected EmailAddressInterface $_mailSecondaire;
	
	/**
	 * A string representing the source of the user.
	 * 
	 * @var string
	 */
	protected string $_modeInscriptionUsager;
	
	/**
	 * The no.secu of the user.
	 * 
	 * @var string
	 */
	protected string $_nir;
	
	/**
	 * The last name of the user.
	 * 
	 * @var string
	 */
	protected string $_nom;
	
	/**
	 * The first name of the user.
	 * 
	 * @var string
	 */
	protected string $_prenom;
	
	/**
	 * The sex code of the user.
	 * 
	 * @var string
	 */
	protected string $_codeSexe;
	
	/**
	 * Constructor for ApiFrGouvEnsapProfileDonnee with private members.
	 * 
	 * @param bool $estAbonneNotification
	 * @param EmailAddressInterface $mailPrincipal
	 * @param EmailAddressInterface $mailSecondaire
	 * @param string $modeInscriptionUsager
	 * @param string $nir
	 * @param string $nom
	 * @param string $prenom
	 * @param string $codeSexe
	 * @SuppressWarnings("PHPMD.ExcessiveParameterList")
	 */
	public function __construct(bool $estAbonneNotification, EmailAddressInterface $mailPrincipal, EmailAddressInterface $mailSecondaire, string $modeInscriptionUsager, string $nir, string $nom, string $prenom, string $codeSexe)
	{
		$this->setEstAbonneNotification($estAbonneNotification);
		$this->setMailPrincipal($mailPrincipal);
		$this->setMailSecondaire($mailSecondaire);
		$this->setModeInscriptionUsager($modeInscriptionUsager);
		$this->setNir($nir);
		$this->setNom($nom);
		$this->setPrenom($prenom);
		$this->setCodeSexe($codeSexe);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets whether the current user has subscribed to notifications.
	 * 
	 * @param bool $estAbonneNotification
	 * @return ApiFrGouvEnsapProfileDonneeInterface
	 */
	public function setEstAbonneNotification(bool $estAbonneNotification) : ApiFrGouvEnsapProfileDonneeInterface
	{
		$this->_estAbonneNotification = $estAbonneNotification;
		
		return $this;
	}
	
	/**
	 * Gets whether the current user has subscribed to notifications.
	 * 
	 * @return bool
	 */
	public function hasEstAbonneNotification() : bool
	{
		return $this->_estAbonneNotification;
	}
	
	/**
	 * Sets whether the current user has subscribed to ensap specific
	 * informations.
	 * 
	 * @param ?bool $estAbonneMailEnsap
	 * @return ApiFrGouvEnsapProfileDonneeInterface
	 */
	public function setEstAbonneMailEnsap(?bool $estAbonneMailEnsap) : ApiFrGouvEnsapProfileDonneeInterface
	{
		$this->_estAbonneMailEnsap = $estAbonneMailEnsap;
		
		return $this;
	}
	
	/**
	 * Gets whether the current user has subscribed to ensap specific
	 * informations.
	 * 
	 * @return ?bool
	 */
	public function hasEstAbonneMailEnsap() : ?bool
	{
		return $this->_estAbonneMailEnsap;
	}
	
	/**
	 * Sets the email address used for notifications.
	 * 
	 * @param ?EmailAddressInterface $mailNotification
	 * @return ApiFrGouvEnsapProfileDonneeInterface
	 */
	public function setMailNotification(?EmailAddressInterface $mailNotification) : ApiFrGouvEnsapProfileDonneeInterface
	{
		$this->_mailNotification = $mailNotification;
		
		return $this;
	}
	
	/**
	 * Gets the email address used for notifications.
	 * 
	 * @return ?EmailAddressInterface
	 */
	public function getMailNotification() : ?EmailAddressInterface
	{
		return $this->_mailNotification;
	}
	
	/**
	 * Sets the email address used as principal.
	 * 
	 * @param EmailAddressInterface $mailPrincipal
	 * @return ApiFrGouvEnsapProfileDonneeInterface
	 */
	public function setMailPrincipal(EmailAddressInterface $mailPrincipal) : ApiFrGouvEnsapProfileDonneeInterface
	{
		$this->_mailPrincipal = $mailPrincipal;
		
		return $this;
	}
	
	/**
	 * Gets the email address used as principal.
	 * 
	 * @return EmailAddressInterface
	 */
	public function getMailPrincipal() : EmailAddressInterface
	{
		return $this->_mailPrincipal;
	}
	
	/**
	 * Sets the email address used as secondary.
	 * 
	 * @param EmailAddressInterface $mailSecondaire
	 * @return ApiFrGouvEnsapProfileDonneeInterface
	 */
	public function setMailSecondaire(EmailAddressInterface $mailSecondaire) : ApiFrGouvEnsapProfileDonneeInterface
	{
		$this->_mailSecondaire = $mailSecondaire;
		
		return $this;
	}
	
	/**
	 * Gets the email address used as secondary.
	 * 
	 * @return EmailAddressInterface
	 */
	public function getMailSecondaire() : EmailAddressInterface
	{
		return $this->_mailSecondaire;
	}
	
	/**
	 * Sets a string representing the source of the user.
	 * 
	 * @param string $modeInscriptionUsager
	 * @return ApiFrGouvEnsapProfileDonneeInterface
	 */
	public function setModeInscriptionUsager(string $modeInscriptionUsager) : ApiFrGouvEnsapProfileDonneeInterface
	{
		$this->_modeInscriptionUsager = $modeInscriptionUsager;
		
		return $this;
	}
	
	/**
	 * Gets a string representing the source of the user.
	 * 
	 * @return string
	 */
	public function getModeInscriptionUsager() : string
	{
		return $this->_modeInscriptionUsager;
	}
	
	/**
	 * Sets the no.secu of the user.
	 * 
	 * @param string $nir
	 * @return ApiFrGouvEnsapProfileDonneeInterface
	 */
	public function setNir(string $nir) : ApiFrGouvEnsapProfileDonneeInterface
	{
		$this->_nir = $nir;
		
		return $this;
	}
	
	/**
	 * Gets the no.secu of the user.
	 * 
	 * @return string
	 */
	public function getNir() : string
	{
		return $this->_nir;
	}
	
	/**
	 * Sets the last name of the user.
	 * 
	 * @param string $nom
	 * @return ApiFrGouvEnsapProfileDonneeInterface
	 */
	public function setNom(string $nom) : ApiFrGouvEnsapProfileDonneeInterface
	{
		$this->_nom = $nom;
		
		return $this;
	}
	
	/**
	 * Gets the last name of the user.
	 * 
	 * @return string
	 */
	public function getNom() : string
	{
		return $this->_nom;
	}
	
	/**
	 * Sets the first name of the user.
	 * 
	 * @param string $prenom
	 * @return ApiFrGouvEnsapProfileDonneeInterface
	 */
	public function setPrenom(string $prenom) : ApiFrGouvEnsapProfileDonneeInterface
	{
		$this->_prenom = $prenom;
		
		return $this;
	}
	
	/**
	 * Gets the first name of the user.
	 * 
	 * @return string
	 */
	public function getPrenom() : string
	{
		return $this->_prenom;
	}
	
	/**
	 * Sets the sex code of the user.
	 * 
	 * @param string $codeSexe
	 * @return ApiFrGouvEnsapProfileDonneeInterface
	 */
	public function setCodeSexe(string $codeSexe) : ApiFrGouvEnsapProfileDonneeInterface
	{
		$this->_codeSexe = $codeSexe;
		
		return $this;
	}
	
	/**
	 * Gets the sex code of the user.
	 * 
	 * @return string
	 */
	public function getCodeSexe() : string
	{
		return $this->_codeSexe;
	}
	
}
