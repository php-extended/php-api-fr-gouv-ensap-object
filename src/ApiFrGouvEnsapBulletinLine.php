<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-ensap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvEnsap;

/**
 * ApiFrGouvEnsapBulletinLine class file.
 * 
 * This is a simple implementation of the ApiFrGouvEnsapBulletinLineInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ApiFrGouvEnsapBulletinLine implements ApiFrGouvEnsapBulletinLineInterface
{
	
	/**
	 * Gets the code of the line.
	 * 
	 * @var ?int
	 */
	protected ?int $_code = null;
	
	/**
	 * Gets the libelle of the line.
	 * 
	 * @var string
	 */
	protected string $_libelle;
	
	/**
	 * Gets the amount to be paid for this line (EUR cts).
	 * 
	 * @var ?int
	 */
	protected ?int $_aPayer = null;
	
	/**
	 * Gets the amount to be deduced from this line (EUR cts).
	 * 
	 * @var ?int
	 */
	protected ?int $_aDeduire = null;
	
	/**
	 * Gets the amount the boss paid for this line (EUR cts).
	 * 
	 * @var ?int
	 */
	protected ?int $_pourInfo = null;
	
	/**
	 * Constructor for ApiFrGouvEnsapBulletinLine with private members.
	 * 
	 * @param string $libelle
	 */
	public function __construct(string $libelle)
	{
		$this->setLibelle($libelle);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets gets the code of the line.
	 * 
	 * @param ?int $code
	 * @return ApiFrGouvEnsapBulletinLineInterface
	 */
	public function setCode(?int $code) : ApiFrGouvEnsapBulletinLineInterface
	{
		$this->_code = $code;
		
		return $this;
	}
	
	/**
	 * Gets gets the code of the line.
	 * 
	 * @return ?int
	 */
	public function getCode() : ?int
	{
		return $this->_code;
	}
	
	/**
	 * Sets gets the libelle of the line.
	 * 
	 * @param string $libelle
	 * @return ApiFrGouvEnsapBulletinLineInterface
	 */
	public function setLibelle(string $libelle) : ApiFrGouvEnsapBulletinLineInterface
	{
		$this->_libelle = $libelle;
		
		return $this;
	}
	
	/**
	 * Gets gets the libelle of the line.
	 * 
	 * @return string
	 */
	public function getLibelle() : string
	{
		return $this->_libelle;
	}
	
	/**
	 * Sets gets the amount to be paid for this line (EUR cts).
	 * 
	 * @param ?int $aPayer
	 * @return ApiFrGouvEnsapBulletinLineInterface
	 */
	public function setAPayer(?int $aPayer) : ApiFrGouvEnsapBulletinLineInterface
	{
		$this->_aPayer = $aPayer;
		
		return $this;
	}
	
	/**
	 * Gets gets the amount to be paid for this line (EUR cts).
	 * 
	 * @return ?int
	 */
	public function getAPayer() : ?int
	{
		return $this->_aPayer;
	}
	
	/**
	 * Sets gets the amount to be deduced from this line (EUR cts).
	 * 
	 * @param ?int $aDeduire
	 * @return ApiFrGouvEnsapBulletinLineInterface
	 */
	public function setADeduire(?int $aDeduire) : ApiFrGouvEnsapBulletinLineInterface
	{
		$this->_aDeduire = $aDeduire;
		
		return $this;
	}
	
	/**
	 * Gets gets the amount to be deduced from this line (EUR cts).
	 * 
	 * @return ?int
	 */
	public function getADeduire() : ?int
	{
		return $this->_aDeduire;
	}
	
	/**
	 * Sets gets the amount the boss paid for this line (EUR cts).
	 * 
	 * @param ?int $pourInfo
	 * @return ApiFrGouvEnsapBulletinLineInterface
	 */
	public function setPourInfo(?int $pourInfo) : ApiFrGouvEnsapBulletinLineInterface
	{
		$this->_pourInfo = $pourInfo;
		
		return $this;
	}
	
	/**
	 * Gets gets the amount the boss paid for this line (EUR cts).
	 * 
	 * @return ?int
	 */
	public function getPourInfo() : ?int
	{
		return $this->_pourInfo;
	}
	
}
