<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-ensap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvEnsap;

/**
 * ApiFrGouvEnsapContenu class file.
 * 
 * This is a simple implementation of the ApiFrGouvEnsapContenuInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ApiFrGouvEnsapContenu implements ApiFrGouvEnsapContenuInterface
{
	
	/**
	 * The messages of the contenu, indexed by field name.
	 * 
	 * @var array<string, string>
	 */
	protected array $_champ = [];
	
	/**
	 * The paragraphes of the contenu, indexed by field name.
	 * 
	 * @var array<string, ApiFrGouvEnsapTextInterface>
	 */
	protected array $_paragraphe = [];
	
	/**
	 * Constructor for ApiFrGouvEnsapContenu with private members.
	 * 
	 * @param array<string, string> $champ
	 * @param array<string, ApiFrGouvEnsapTextInterface> $paragraphe
	 */
	public function __construct(array $champ, array $paragraphe)
	{
		$this->setChamp($champ);
		$this->setParagraphe($paragraphe);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the messages of the contenu, indexed by field name.
	 * 
	 * @param array<string, string> $champ
	 * @return ApiFrGouvEnsapContenuInterface
	 */
	public function setChamp(array $champ) : ApiFrGouvEnsapContenuInterface
	{
		$this->_champ = $champ;
		
		return $this;
	}
	
	/**
	 * Gets the messages of the contenu, indexed by field name.
	 * 
	 * @return array<string, string>
	 */
	public function getChamp() : array
	{
		return $this->_champ;
	}
	
	/**
	 * Sets the paragraphes of the contenu, indexed by field name.
	 * 
	 * @param array<string, ApiFrGouvEnsapTextInterface> $paragraphe
	 * @return ApiFrGouvEnsapContenuInterface
	 */
	public function setParagraphe(array $paragraphe) : ApiFrGouvEnsapContenuInterface
	{
		$this->_paragraphe = $paragraphe;
		
		return $this;
	}
	
	/**
	 * Gets the paragraphes of the contenu, indexed by field name.
	 * 
	 * @return array<string, ApiFrGouvEnsapTextInterface>
	 */
	public function getParagraphe() : array
	{
		return $this->_paragraphe;
	}
	
}
