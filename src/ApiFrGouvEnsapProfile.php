<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-ensap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvEnsap;

/**
 * ApiFrGouvEnsapProfile class file.
 * 
 * This is a simple implementation of the ApiFrGouvEnsapProfileInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ApiFrGouvEnsapProfile implements ApiFrGouvEnsapProfileInterface
{
	
	/**
	 * The contenu of this help texts for this profile.
	 * 
	 * @var ApiFrGouvEnsapContenuInterface
	 */
	protected ApiFrGouvEnsapContenuInterface $_contenu;
	
	/**
	 * The data about the logged user.
	 * 
	 * @var ApiFrGouvEnsapProfileDonneeInterface
	 */
	protected ApiFrGouvEnsapProfileDonneeInterface $_donnee;
	
	/**
	 * All the translations for the available messages.
	 * 
	 * @var array<string, string>
	 */
	protected array $_message = [];
	
	/**
	 * All the translations for alert messages.
	 * 
	 * @var array<string, string>
	 */
	protected array $_messagealerte = [];
	
	/**
	 * All the parameters.
	 * 
	 * @var array<string, string>
	 */
	protected array $_parametrage = [];
	
	/**
	 * Constructor for ApiFrGouvEnsapProfile with private members.
	 * 
	 * @param ApiFrGouvEnsapContenuInterface $contenu
	 * @param ApiFrGouvEnsapProfileDonneeInterface $donnee
	 * @param array<string, string> $message
	 * @param array<string, string> $messagealerte
	 * @param array<string, string> $parametrage
	 */
	public function __construct(ApiFrGouvEnsapContenuInterface $contenu, ApiFrGouvEnsapProfileDonneeInterface $donnee, array $message, array $messagealerte, array $parametrage)
	{
		$this->setContenu($contenu);
		$this->setDonnee($donnee);
		$this->setMessage($message);
		$this->setMessagealerte($messagealerte);
		$this->setParametrage($parametrage);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the contenu of this help texts for this profile.
	 * 
	 * @param ApiFrGouvEnsapContenuInterface $contenu
	 * @return ApiFrGouvEnsapProfileInterface
	 */
	public function setContenu(ApiFrGouvEnsapContenuInterface $contenu) : ApiFrGouvEnsapProfileInterface
	{
		$this->_contenu = $contenu;
		
		return $this;
	}
	
	/**
	 * Gets the contenu of this help texts for this profile.
	 * 
	 * @return ApiFrGouvEnsapContenuInterface
	 */
	public function getContenu() : ApiFrGouvEnsapContenuInterface
	{
		return $this->_contenu;
	}
	
	/**
	 * Sets the data about the logged user.
	 * 
	 * @param ApiFrGouvEnsapProfileDonneeInterface $donnee
	 * @return ApiFrGouvEnsapProfileInterface
	 */
	public function setDonnee(ApiFrGouvEnsapProfileDonneeInterface $donnee) : ApiFrGouvEnsapProfileInterface
	{
		$this->_donnee = $donnee;
		
		return $this;
	}
	
	/**
	 * Gets the data about the logged user.
	 * 
	 * @return ApiFrGouvEnsapProfileDonneeInterface
	 */
	public function getDonnee() : ApiFrGouvEnsapProfileDonneeInterface
	{
		return $this->_donnee;
	}
	
	/**
	 * Sets all the translations for the available messages.
	 * 
	 * @param array<string, string> $message
	 * @return ApiFrGouvEnsapProfileInterface
	 */
	public function setMessage(array $message) : ApiFrGouvEnsapProfileInterface
	{
		$this->_message = $message;
		
		return $this;
	}
	
	/**
	 * Gets all the translations for the available messages.
	 * 
	 * @return array<string, string>
	 */
	public function getMessage() : array
	{
		return $this->_message;
	}
	
	/**
	 * Sets all the translations for alert messages.
	 * 
	 * @param array<string, string> $messagealerte
	 * @return ApiFrGouvEnsapProfileInterface
	 */
	public function setMessagealerte(array $messagealerte) : ApiFrGouvEnsapProfileInterface
	{
		$this->_messagealerte = $messagealerte;
		
		return $this;
	}
	
	/**
	 * Gets all the translations for alert messages.
	 * 
	 * @return array<string, string>
	 */
	public function getMessagealerte() : array
	{
		return $this->_messagealerte;
	}
	
	/**
	 * Sets all the parameters.
	 * 
	 * @param array<string, string> $parametrage
	 * @return ApiFrGouvEnsapProfileInterface
	 */
	public function setParametrage(array $parametrage) : ApiFrGouvEnsapProfileInterface
	{
		$this->_parametrage = $parametrage;
		
		return $this;
	}
	
	/**
	 * Gets all the parameters.
	 * 
	 * @return array<string, string>
	 */
	public function getParametrage() : array
	{
		return $this->_parametrage;
	}
	
}
