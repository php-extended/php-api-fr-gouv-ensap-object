<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-ensap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvEnsap;

use DateTimeInterface;

/**
 * ApiFrGouvEnsapBulletin class file.
 * 
 * This is a simple implementation of the ApiFrGouvEnsapBulletinInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ExcessiveClassComplexity")
 * @SuppressWarnings("PHPMD.ExcessiveClassLength")
 * @SuppressWarnings("PHPMD.ExcessivePublicCount")
 * @SuppressWarnings("PHPMD.TooManyFields")
 * @SuppressWarnings("PHPMD.TooManyPublicMethods")
 */
class ApiFrGouvEnsapBulletin implements ApiFrGouvEnsapBulletinInterface
{
	
	/**
	 * The emitter of the bulletin.
	 * 
	 * @var ?string
	 */
	protected ?string $_emitter = null;
	
	/**
	 * The month of emission, month in letters, year in digits.
	 * 
	 * @var ?string
	 */
	protected ?string $_monthLetter = null;
	
	/**
	 * The numero of the order to be paid.
	 * 
	 * @var ?string
	 */
	protected ?string $_numOrder = null;
	
	/**
	 * The quantity of worked time.
	 * 
	 * @var ?string
	 */
	protected ?string $_workTime = null;
	
	/**
	 * The number of the affectation place (first line).
	 * 
	 * @var ?string
	 */
	protected ?string $_poste1 = null;
	
	/**
	 * The number of the affectation place (second line).
	 * 
	 * @var ?string
	 */
	protected ?string $_poste2 = null;
	
	/**
	 * The label of the affectation place.
	 * 
	 * @var ?string
	 */
	protected ?string $_libellePoste = null;
	
	/**
	 * The siret number of the employer.
	 * 
	 * @var ?string
	 */
	protected ?string $_siretEmployeur = null;
	
	/**
	 * The ministry number of the agent.
	 * 
	 * @var ?int
	 */
	protected ?int $_idMin = null;
	
	/**
	 * The no secu of the agent.
	 * 
	 * @var ?string
	 */
	protected ?string $_idNir = null;
	
	/**
	 * The no dossier of the agent.
	 * 
	 * @var ?int
	 */
	protected ?int $_idNodos = null;
	
	/**
	 * The grade of the agent.
	 * 
	 * @var ?string
	 */
	protected ?string $_grade = null;
	
	/**
	 * The number of children the agent has at charge.
	 * 
	 * @var ?int
	 */
	protected ?int $_childCharged = null;
	
	/**
	 * The echelon of the agent.
	 * 
	 * @var ?string
	 */
	protected ?string $_echelon = null;
	
	/**
	 * The index of the agent (or effective number of hours worked).
	 * 
	 * @var ?int
	 */
	protected ?int $_indiceOrNbHours = null;
	
	/**
	 * The hour rate of payment.
	 * 
	 * @var ?int
	 */
	protected ?int $_tauxHoraire = null;
	
	/**
	 * Whether the agent is at part time.
	 * 
	 * @var ?string
	 */
	protected ?string $_tpsPartiel = null;
	
	/**
	 * The lines of payment on the bulletin.
	 * 
	 * @var array<int, ApiFrGouvEnsapBulletinLineInterface>
	 */
	protected array $_lines = [];
	
	/**
	 * The total amount to be paid (EUR cts).
	 * 
	 * @var ?int
	 */
	protected ?int $_totalAPayer = null;
	
	/**
	 * The total amount to be deduced (EUR cts).
	 * 
	 * @var ?int
	 */
	protected ?int $_totalADeduire = null;
	
	/**
	 * The total taxes to be paid by the employer (EUR cts).
	 * 
	 * @var ?int
	 */
	protected ?int $_totalPourInfo = null;
	
	/**
	 * The total amount to be paid by the employer (EUR cts).
	 * 
	 * @var ?int
	 */
	protected ?int $_totalEmployeur = null;
	
	/**
	 * The total net amount to receive (EUR cts).
	 * 
	 * @var ?int
	 */
	protected ?int $_totalNetAPayer = null;
	
	/**
	 * The base ss for the year (EUR cts).
	 * 
	 * @var ?int
	 */
	protected ?int $_baseSsYear = null;
	
	/**
	 * The base ss for the month (EUR cts).
	 * 
	 * @var ?int
	 */
	protected ?int $_baseSsMonth = null;
	
	/**
	 * The montant imposable for the year (EUR cts).
	 * 
	 * @var ?int
	 */
	protected ?int $_montantImposableYear = null;
	
	/**
	 * The montant imposable for the month (EUR cts).
	 * 
	 * @var ?int
	 */
	protected ?int $_montahtImposableMonth = null;
	
	/**
	 * The assignated comptable office.
	 * 
	 * @var ?string
	 */
	protected ?string $_comptable = null;
	
	/**
	 * When this bulletin was paid.
	 * 
	 * @var ?DateTimeInterface
	 */
	protected ?DateTimeInterface $_datePaid = null;
	
	/**
	 * The iban of the account this bulletin was paid to.
	 * 
	 * @var ?string
	 */
	protected ?string $_accountPaidIban = null;
	
	/**
	 * The bic of the account this bulletin was paid to.
	 * 
	 * @var ?string
	 */
	protected ?string $_accountPaidBic = null;
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the emitter of the bulletin.
	 * 
	 * @param ?string $emitter
	 * @return ApiFrGouvEnsapBulletinInterface
	 */
	public function setEmitter(?string $emitter) : ApiFrGouvEnsapBulletinInterface
	{
		$this->_emitter = $emitter;
		
		return $this;
	}
	
	/**
	 * Gets the emitter of the bulletin.
	 * 
	 * @return ?string
	 */
	public function getEmitter() : ?string
	{
		return $this->_emitter;
	}
	
	/**
	 * Sets the month of emission, month in letters, year in digits.
	 * 
	 * @param ?string $monthLetter
	 * @return ApiFrGouvEnsapBulletinInterface
	 */
	public function setMonthLetter(?string $monthLetter) : ApiFrGouvEnsapBulletinInterface
	{
		$this->_monthLetter = $monthLetter;
		
		return $this;
	}
	
	/**
	 * Gets the month of emission, month in letters, year in digits.
	 * 
	 * @return ?string
	 */
	public function getMonthLetter() : ?string
	{
		return $this->_monthLetter;
	}
	
	/**
	 * Sets the numero of the order to be paid.
	 * 
	 * @param ?string $numOrder
	 * @return ApiFrGouvEnsapBulletinInterface
	 */
	public function setNumOrder(?string $numOrder) : ApiFrGouvEnsapBulletinInterface
	{
		$this->_numOrder = $numOrder;
		
		return $this;
	}
	
	/**
	 * Gets the numero of the order to be paid.
	 * 
	 * @return ?string
	 */
	public function getNumOrder() : ?string
	{
		return $this->_numOrder;
	}
	
	/**
	 * Sets the quantity of worked time.
	 * 
	 * @param ?string $workTime
	 * @return ApiFrGouvEnsapBulletinInterface
	 */
	public function setWorkTime(?string $workTime) : ApiFrGouvEnsapBulletinInterface
	{
		$this->_workTime = $workTime;
		
		return $this;
	}
	
	/**
	 * Gets the quantity of worked time.
	 * 
	 * @return ?string
	 */
	public function getWorkTime() : ?string
	{
		return $this->_workTime;
	}
	
	/**
	 * Sets the number of the affectation place (first line).
	 * 
	 * @param ?string $poste1
	 * @return ApiFrGouvEnsapBulletinInterface
	 */
	public function setPoste1(?string $poste1) : ApiFrGouvEnsapBulletinInterface
	{
		$this->_poste1 = $poste1;
		
		return $this;
	}
	
	/**
	 * Gets the number of the affectation place (first line).
	 * 
	 * @return ?string
	 */
	public function getPoste1() : ?string
	{
		return $this->_poste1;
	}
	
	/**
	 * Sets the number of the affectation place (second line).
	 * 
	 * @param ?string $poste2
	 * @return ApiFrGouvEnsapBulletinInterface
	 */
	public function setPoste2(?string $poste2) : ApiFrGouvEnsapBulletinInterface
	{
		$this->_poste2 = $poste2;
		
		return $this;
	}
	
	/**
	 * Gets the number of the affectation place (second line).
	 * 
	 * @return ?string
	 */
	public function getPoste2() : ?string
	{
		return $this->_poste2;
	}
	
	/**
	 * Sets the label of the affectation place.
	 * 
	 * @param ?string $libellePoste
	 * @return ApiFrGouvEnsapBulletinInterface
	 */
	public function setLibellePoste(?string $libellePoste) : ApiFrGouvEnsapBulletinInterface
	{
		$this->_libellePoste = $libellePoste;
		
		return $this;
	}
	
	/**
	 * Gets the label of the affectation place.
	 * 
	 * @return ?string
	 */
	public function getLibellePoste() : ?string
	{
		return $this->_libellePoste;
	}
	
	/**
	 * Sets the siret number of the employer.
	 * 
	 * @param ?string $siretEmployeur
	 * @return ApiFrGouvEnsapBulletinInterface
	 */
	public function setSiretEmployeur(?string $siretEmployeur) : ApiFrGouvEnsapBulletinInterface
	{
		$this->_siretEmployeur = $siretEmployeur;
		
		return $this;
	}
	
	/**
	 * Gets the siret number of the employer.
	 * 
	 * @return ?string
	 */
	public function getSiretEmployeur() : ?string
	{
		return $this->_siretEmployeur;
	}
	
	/**
	 * Sets the ministry number of the agent.
	 * 
	 * @param ?int $idMin
	 * @return ApiFrGouvEnsapBulletinInterface
	 */
	public function setIdMin(?int $idMin) : ApiFrGouvEnsapBulletinInterface
	{
		$this->_idMin = $idMin;
		
		return $this;
	}
	
	/**
	 * Gets the ministry number of the agent.
	 * 
	 * @return ?int
	 */
	public function getIdMin() : ?int
	{
		return $this->_idMin;
	}
	
	/**
	 * Sets the no secu of the agent.
	 * 
	 * @param ?string $idNir
	 * @return ApiFrGouvEnsapBulletinInterface
	 */
	public function setIdNir(?string $idNir) : ApiFrGouvEnsapBulletinInterface
	{
		$this->_idNir = $idNir;
		
		return $this;
	}
	
	/**
	 * Gets the no secu of the agent.
	 * 
	 * @return ?string
	 */
	public function getIdNir() : ?string
	{
		return $this->_idNir;
	}
	
	/**
	 * Sets the no dossier of the agent.
	 * 
	 * @param ?int $idNodos
	 * @return ApiFrGouvEnsapBulletinInterface
	 */
	public function setIdNodos(?int $idNodos) : ApiFrGouvEnsapBulletinInterface
	{
		$this->_idNodos = $idNodos;
		
		return $this;
	}
	
	/**
	 * Gets the no dossier of the agent.
	 * 
	 * @return ?int
	 */
	public function getIdNodos() : ?int
	{
		return $this->_idNodos;
	}
	
	/**
	 * Sets the grade of the agent.
	 * 
	 * @param ?string $grade
	 * @return ApiFrGouvEnsapBulletinInterface
	 */
	public function setGrade(?string $grade) : ApiFrGouvEnsapBulletinInterface
	{
		$this->_grade = $grade;
		
		return $this;
	}
	
	/**
	 * Gets the grade of the agent.
	 * 
	 * @return ?string
	 */
	public function getGrade() : ?string
	{
		return $this->_grade;
	}
	
	/**
	 * Sets the number of children the agent has at charge.
	 * 
	 * @param ?int $childCharged
	 * @return ApiFrGouvEnsapBulletinInterface
	 */
	public function setChildCharged(?int $childCharged) : ApiFrGouvEnsapBulletinInterface
	{
		$this->_childCharged = $childCharged;
		
		return $this;
	}
	
	/**
	 * Gets the number of children the agent has at charge.
	 * 
	 * @return ?int
	 */
	public function getChildCharged() : ?int
	{
		return $this->_childCharged;
	}
	
	/**
	 * Sets the echelon of the agent.
	 * 
	 * @param ?string $echelon
	 * @return ApiFrGouvEnsapBulletinInterface
	 */
	public function setEchelon(?string $echelon) : ApiFrGouvEnsapBulletinInterface
	{
		$this->_echelon = $echelon;
		
		return $this;
	}
	
	/**
	 * Gets the echelon of the agent.
	 * 
	 * @return ?string
	 */
	public function getEchelon() : ?string
	{
		return $this->_echelon;
	}
	
	/**
	 * Sets the index of the agent (or effective number of hours worked).
	 * 
	 * @param ?int $indiceOrNbHours
	 * @return ApiFrGouvEnsapBulletinInterface
	 */
	public function setIndiceOrNbHours(?int $indiceOrNbHours) : ApiFrGouvEnsapBulletinInterface
	{
		$this->_indiceOrNbHours = $indiceOrNbHours;
		
		return $this;
	}
	
	/**
	 * Gets the index of the agent (or effective number of hours worked).
	 * 
	 * @return ?int
	 */
	public function getIndiceOrNbHours() : ?int
	{
		return $this->_indiceOrNbHours;
	}
	
	/**
	 * Sets the hour rate of payment.
	 * 
	 * @param ?int $tauxHoraire
	 * @return ApiFrGouvEnsapBulletinInterface
	 */
	public function setTauxHoraire(?int $tauxHoraire) : ApiFrGouvEnsapBulletinInterface
	{
		$this->_tauxHoraire = $tauxHoraire;
		
		return $this;
	}
	
	/**
	 * Gets the hour rate of payment.
	 * 
	 * @return ?int
	 */
	public function getTauxHoraire() : ?int
	{
		return $this->_tauxHoraire;
	}
	
	/**
	 * Sets whether the agent is at part time.
	 * 
	 * @param ?string $tpsPartiel
	 * @return ApiFrGouvEnsapBulletinInterface
	 */
	public function setTpsPartiel(?string $tpsPartiel) : ApiFrGouvEnsapBulletinInterface
	{
		$this->_tpsPartiel = $tpsPartiel;
		
		return $this;
	}
	
	/**
	 * Gets whether the agent is at part time.
	 * 
	 * @return ?string
	 */
	public function getTpsPartiel() : ?string
	{
		return $this->_tpsPartiel;
	}
	
	/**
	 * Sets the lines of payment on the bulletin.
	 * 
	 * @param array<int, ApiFrGouvEnsapBulletinLineInterface> $lines
	 * @return ApiFrGouvEnsapBulletinInterface
	 */
	public function setLines(array $lines) : ApiFrGouvEnsapBulletinInterface
	{
		$this->_lines = $lines;
		
		return $this;
	}
	
	/**
	 * Gets the lines of payment on the bulletin.
	 * 
	 * @return array<int, ApiFrGouvEnsapBulletinLineInterface>
	 */
	public function getLines() : array
	{
		return $this->_lines;
	}
	
	/**
	 * Sets the total amount to be paid (EUR cts).
	 * 
	 * @param ?int $totalAPayer
	 * @return ApiFrGouvEnsapBulletinInterface
	 */
	public function setTotalAPayer(?int $totalAPayer) : ApiFrGouvEnsapBulletinInterface
	{
		$this->_totalAPayer = $totalAPayer;
		
		return $this;
	}
	
	/**
	 * Gets the total amount to be paid (EUR cts).
	 * 
	 * @return ?int
	 */
	public function getTotalAPayer() : ?int
	{
		return $this->_totalAPayer;
	}
	
	/**
	 * Sets the total amount to be deduced (EUR cts).
	 * 
	 * @param ?int $totalADeduire
	 * @return ApiFrGouvEnsapBulletinInterface
	 */
	public function setTotalADeduire(?int $totalADeduire) : ApiFrGouvEnsapBulletinInterface
	{
		$this->_totalADeduire = $totalADeduire;
		
		return $this;
	}
	
	/**
	 * Gets the total amount to be deduced (EUR cts).
	 * 
	 * @return ?int
	 */
	public function getTotalADeduire() : ?int
	{
		return $this->_totalADeduire;
	}
	
	/**
	 * Sets the total taxes to be paid by the employer (EUR cts).
	 * 
	 * @param ?int $totalPourInfo
	 * @return ApiFrGouvEnsapBulletinInterface
	 */
	public function setTotalPourInfo(?int $totalPourInfo) : ApiFrGouvEnsapBulletinInterface
	{
		$this->_totalPourInfo = $totalPourInfo;
		
		return $this;
	}
	
	/**
	 * Gets the total taxes to be paid by the employer (EUR cts).
	 * 
	 * @return ?int
	 */
	public function getTotalPourInfo() : ?int
	{
		return $this->_totalPourInfo;
	}
	
	/**
	 * Sets the total amount to be paid by the employer (EUR cts).
	 * 
	 * @param ?int $totalEmployeur
	 * @return ApiFrGouvEnsapBulletinInterface
	 */
	public function setTotalEmployeur(?int $totalEmployeur) : ApiFrGouvEnsapBulletinInterface
	{
		$this->_totalEmployeur = $totalEmployeur;
		
		return $this;
	}
	
	/**
	 * Gets the total amount to be paid by the employer (EUR cts).
	 * 
	 * @return ?int
	 */
	public function getTotalEmployeur() : ?int
	{
		return $this->_totalEmployeur;
	}
	
	/**
	 * Sets the total net amount to receive (EUR cts).
	 * 
	 * @param ?int $totalNetAPayer
	 * @return ApiFrGouvEnsapBulletinInterface
	 */
	public function setTotalNetAPayer(?int $totalNetAPayer) : ApiFrGouvEnsapBulletinInterface
	{
		$this->_totalNetAPayer = $totalNetAPayer;
		
		return $this;
	}
	
	/**
	 * Gets the total net amount to receive (EUR cts).
	 * 
	 * @return ?int
	 */
	public function getTotalNetAPayer() : ?int
	{
		return $this->_totalNetAPayer;
	}
	
	/**
	 * Sets the base ss for the year (EUR cts).
	 * 
	 * @param ?int $baseSsYear
	 * @return ApiFrGouvEnsapBulletinInterface
	 */
	public function setBaseSsYear(?int $baseSsYear) : ApiFrGouvEnsapBulletinInterface
	{
		$this->_baseSsYear = $baseSsYear;
		
		return $this;
	}
	
	/**
	 * Gets the base ss for the year (EUR cts).
	 * 
	 * @return ?int
	 */
	public function getBaseSsYear() : ?int
	{
		return $this->_baseSsYear;
	}
	
	/**
	 * Sets the base ss for the month (EUR cts).
	 * 
	 * @param ?int $baseSsMonth
	 * @return ApiFrGouvEnsapBulletinInterface
	 */
	public function setBaseSsMonth(?int $baseSsMonth) : ApiFrGouvEnsapBulletinInterface
	{
		$this->_baseSsMonth = $baseSsMonth;
		
		return $this;
	}
	
	/**
	 * Gets the base ss for the month (EUR cts).
	 * 
	 * @return ?int
	 */
	public function getBaseSsMonth() : ?int
	{
		return $this->_baseSsMonth;
	}
	
	/**
	 * Sets the montant imposable for the year (EUR cts).
	 * 
	 * @param ?int $montantImposableYear
	 * @return ApiFrGouvEnsapBulletinInterface
	 */
	public function setMontantImposableYear(?int $montantImposableYear) : ApiFrGouvEnsapBulletinInterface
	{
		$this->_montantImposableYear = $montantImposableYear;
		
		return $this;
	}
	
	/**
	 * Gets the montant imposable for the year (EUR cts).
	 * 
	 * @return ?int
	 */
	public function getMontantImposableYear() : ?int
	{
		return $this->_montantImposableYear;
	}
	
	/**
	 * Sets the montant imposable for the month (EUR cts).
	 * 
	 * @param ?int $montahtImposableMonth
	 * @return ApiFrGouvEnsapBulletinInterface
	 */
	public function setMontahtImposableMonth(?int $montahtImposableMonth) : ApiFrGouvEnsapBulletinInterface
	{
		$this->_montahtImposableMonth = $montahtImposableMonth;
		
		return $this;
	}
	
	/**
	 * Gets the montant imposable for the month (EUR cts).
	 * 
	 * @return ?int
	 */
	public function getMontahtImposableMonth() : ?int
	{
		return $this->_montahtImposableMonth;
	}
	
	/**
	 * Sets the assignated comptable office.
	 * 
	 * @param ?string $comptable
	 * @return ApiFrGouvEnsapBulletinInterface
	 */
	public function setComptable(?string $comptable) : ApiFrGouvEnsapBulletinInterface
	{
		$this->_comptable = $comptable;
		
		return $this;
	}
	
	/**
	 * Gets the assignated comptable office.
	 * 
	 * @return ?string
	 */
	public function getComptable() : ?string
	{
		return $this->_comptable;
	}
	
	/**
	 * Sets when this bulletin was paid.
	 * 
	 * @param ?DateTimeInterface $datePaid
	 * @return ApiFrGouvEnsapBulletinInterface
	 */
	public function setDatePaid(?DateTimeInterface $datePaid) : ApiFrGouvEnsapBulletinInterface
	{
		$this->_datePaid = $datePaid;
		
		return $this;
	}
	
	/**
	 * Gets when this bulletin was paid.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getDatePaid() : ?DateTimeInterface
	{
		return $this->_datePaid;
	}
	
	/**
	 * Sets the iban of the account this bulletin was paid to.
	 * 
	 * @param ?string $accountPaidIban
	 * @return ApiFrGouvEnsapBulletinInterface
	 */
	public function setAccountPaidIban(?string $accountPaidIban) : ApiFrGouvEnsapBulletinInterface
	{
		$this->_accountPaidIban = $accountPaidIban;
		
		return $this;
	}
	
	/**
	 * Gets the iban of the account this bulletin was paid to.
	 * 
	 * @return ?string
	 */
	public function getAccountPaidIban() : ?string
	{
		return $this->_accountPaidIban;
	}
	
	/**
	 * Sets the bic of the account this bulletin was paid to.
	 * 
	 * @param ?string $accountPaidBic
	 * @return ApiFrGouvEnsapBulletinInterface
	 */
	public function setAccountPaidBic(?string $accountPaidBic) : ApiFrGouvEnsapBulletinInterface
	{
		$this->_accountPaidBic = $accountPaidBic;
		
		return $this;
	}
	
	/**
	 * Gets the bic of the account this bulletin was paid to.
	 * 
	 * @return ?string
	 */
	public function getAccountPaidBic() : ?string
	{
		return $this->_accountPaidBic;
	}
	
}
