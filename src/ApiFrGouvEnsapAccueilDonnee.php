<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-ensap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvEnsap;

/**
 * ApiFrGouvEnsapAccueilDonnee class file.
 * 
 * This is a simple implementation of the ApiFrGouvEnsapAccueilDonneeInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ApiFrGouvEnsapAccueilDonnee implements ApiFrGouvEnsapAccueilDonneeInterface
{
	
	/**
	 * The information of identification of the user.
	 * 
	 * @var ApiFrGouvEnsapIdentificationInterface
	 */
	protected ApiFrGouvEnsapIdentificationInterface $_identification;
	
	/**
	 * The list of events for the user.
	 * 
	 * @var array<int, ApiFrGouvEnsapEvenementInterface>
	 */
	protected array $_listeEvenement = [];
	
	/**
	 * The list of years the event are dispatched on.
	 * 
	 * @var array<int, int>
	 */
	protected array $_listeAnneeRemuneration = [];
	
	/**
	 * Constructor for ApiFrGouvEnsapAccueilDonnee with private members.
	 * 
	 * @param ApiFrGouvEnsapIdentificationInterface $identification
	 * @param array<int, ApiFrGouvEnsapEvenementInterface> $listeEvenement
	 * @param array<int, int> $listeAnneeRemuneration
	 */
	public function __construct(ApiFrGouvEnsapIdentificationInterface $identification, array $listeEvenement, array $listeAnneeRemuneration)
	{
		$this->setIdentification($identification);
		$this->setListeEvenement($listeEvenement);
		$this->setListeAnneeRemuneration($listeAnneeRemuneration);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the information of identification of the user.
	 * 
	 * @param ApiFrGouvEnsapIdentificationInterface $identification
	 * @return ApiFrGouvEnsapAccueilDonneeInterface
	 */
	public function setIdentification(ApiFrGouvEnsapIdentificationInterface $identification) : ApiFrGouvEnsapAccueilDonneeInterface
	{
		$this->_identification = $identification;
		
		return $this;
	}
	
	/**
	 * Gets the information of identification of the user.
	 * 
	 * @return ApiFrGouvEnsapIdentificationInterface
	 */
	public function getIdentification() : ApiFrGouvEnsapIdentificationInterface
	{
		return $this->_identification;
	}
	
	/**
	 * Sets the list of events for the user.
	 * 
	 * @param array<int, ApiFrGouvEnsapEvenementInterface> $listeEvenement
	 * @return ApiFrGouvEnsapAccueilDonneeInterface
	 */
	public function setListeEvenement(array $listeEvenement) : ApiFrGouvEnsapAccueilDonneeInterface
	{
		$this->_listeEvenement = $listeEvenement;
		
		return $this;
	}
	
	/**
	 * Gets the list of events for the user.
	 * 
	 * @return array<int, ApiFrGouvEnsapEvenementInterface>
	 */
	public function getListeEvenement() : array
	{
		return $this->_listeEvenement;
	}
	
	/**
	 * Sets the list of years the event are dispatched on.
	 * 
	 * @param array<int, int> $listeAnneeRemuneration
	 * @return ApiFrGouvEnsapAccueilDonneeInterface
	 */
	public function setListeAnneeRemuneration(array $listeAnneeRemuneration) : ApiFrGouvEnsapAccueilDonneeInterface
	{
		$this->_listeAnneeRemuneration = $listeAnneeRemuneration;
		
		return $this;
	}
	
	/**
	 * Gets the list of years the event are dispatched on.
	 * 
	 * @return array<int, int>
	 */
	public function getListeAnneeRemuneration() : array
	{
		return $this->_listeAnneeRemuneration;
	}
	
}
