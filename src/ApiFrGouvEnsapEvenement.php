<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-ensap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvEnsap;

use DateTimeInterface;
use PhpExtended\Uuid\UuidInterface;

/**
 * ApiFrGouvEnsapEvenement class file.
 * 
 * This is a simple implementation of the ApiFrGouvEnsapEvenementInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ApiFrGouvEnsapEvenement implements ApiFrGouvEnsapEvenementInterface
{
	
	/**
	 * The id of the event.
	 * 
	 * @var int
	 */
	protected int $_evenementId;
	
	/**
	 * The date of the event.
	 * 
	 * @var string
	 */
	protected string $_dateEvenement;
	
	/**
	 * The uuid of the document relative to this event.
	 * 
	 * @var UuidInterface
	 */
	protected UuidInterface $_documentUuid;
	
	/**
	 * The first label of the event.
	 * 
	 * @var string
	 */
	protected string $_libelle1;
	
	/**
	 * The second label of the event.
	 * 
	 * @var ?string
	 */
	protected ?string $_libelle2 = null;
	
	/**
	 * The third label of the event.
	 * 
	 * @var ?string
	 */
	protected ?string $_libelle3 = null;
	
	/**
	 * The statut of the document. May be 'EVE_LU', meaning the document has
	 * been read, or 'EVE_NON_LU', meaning the document has not been read.
	 * 
	 * @var string
	 */
	protected string $_statut;
	
	/**
	 * The date when this document was read.
	 * 
	 * @var ?DateTimeInterface
	 */
	protected ?DateTimeInterface $_dateLecture = null;
	
	/**
	 * The label for the icon of the event.
	 * 
	 * @var ?string
	 */
	protected ?string $_libelleIcone = null;
	
	/**
	 * The id of the icon for the event.
	 * 
	 * @var ?string
	 */
	protected ?string $_icone = null;
	
	/**
	 * The sort number ordering.
	 * 
	 * @var ?int
	 */
	protected ?int $_tri = null;
	
	/**
	 * The action that should be launched wien clicked on this event.
	 * 
	 * @var string
	 */
	protected string $_actionIhm;
	
	/**
	 * The service of the event.
	 * 
	 * @var string
	 */
	protected string $_service;
	
	/**
	 * Constructor for ApiFrGouvEnsapEvenement with private members.
	 * 
	 * @param int $evenementId
	 * @param string $dateEvenement
	 * @param UuidInterface $documentUuid
	 * @param string $libelle1
	 * @param string $statut
	 * @param string $actionIhm
	 * @param string $service
	 * @SuppressWarnings("PHPMD.ExcessiveParameterList")
	 */
	public function __construct(int $evenementId, string $dateEvenement, UuidInterface $documentUuid, string $libelle1, string $statut, string $actionIhm, string $service)
	{
		$this->setEvenementId($evenementId);
		$this->setDateEvenement($dateEvenement);
		$this->setDocumentUuid($documentUuid);
		$this->setLibelle1($libelle1);
		$this->setStatut($statut);
		$this->setActionIhm($actionIhm);
		$this->setService($service);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the id of the event.
	 * 
	 * @param int $evenementId
	 * @return ApiFrGouvEnsapEvenementInterface
	 */
	public function setEvenementId(int $evenementId) : ApiFrGouvEnsapEvenementInterface
	{
		$this->_evenementId = $evenementId;
		
		return $this;
	}
	
	/**
	 * Gets the id of the event.
	 * 
	 * @return int
	 */
	public function getEvenementId() : int
	{
		return $this->_evenementId;
	}
	
	/**
	 * Sets the date of the event.
	 * 
	 * @param string $dateEvenement
	 * @return ApiFrGouvEnsapEvenementInterface
	 */
	public function setDateEvenement(string $dateEvenement) : ApiFrGouvEnsapEvenementInterface
	{
		$this->_dateEvenement = $dateEvenement;
		
		return $this;
	}
	
	/**
	 * Gets the date of the event.
	 * 
	 * @return string
	 */
	public function getDateEvenement() : string
	{
		return $this->_dateEvenement;
	}
	
	/**
	 * Sets the uuid of the document relative to this event.
	 * 
	 * @param UuidInterface $documentUuid
	 * @return ApiFrGouvEnsapEvenementInterface
	 */
	public function setDocumentUuid(UuidInterface $documentUuid) : ApiFrGouvEnsapEvenementInterface
	{
		$this->_documentUuid = $documentUuid;
		
		return $this;
	}
	
	/**
	 * Gets the uuid of the document relative to this event.
	 * 
	 * @return UuidInterface
	 */
	public function getDocumentUuid() : UuidInterface
	{
		return $this->_documentUuid;
	}
	
	/**
	 * Sets the first label of the event.
	 * 
	 * @param string $libelle1
	 * @return ApiFrGouvEnsapEvenementInterface
	 */
	public function setLibelle1(string $libelle1) : ApiFrGouvEnsapEvenementInterface
	{
		$this->_libelle1 = $libelle1;
		
		return $this;
	}
	
	/**
	 * Gets the first label of the event.
	 * 
	 * @return string
	 */
	public function getLibelle1() : string
	{
		return $this->_libelle1;
	}
	
	/**
	 * Sets the second label of the event.
	 * 
	 * @param ?string $libelle2
	 * @return ApiFrGouvEnsapEvenementInterface
	 */
	public function setLibelle2(?string $libelle2) : ApiFrGouvEnsapEvenementInterface
	{
		$this->_libelle2 = $libelle2;
		
		return $this;
	}
	
	/**
	 * Gets the second label of the event.
	 * 
	 * @return ?string
	 */
	public function getLibelle2() : ?string
	{
		return $this->_libelle2;
	}
	
	/**
	 * Sets the third label of the event.
	 * 
	 * @param ?string $libelle3
	 * @return ApiFrGouvEnsapEvenementInterface
	 */
	public function setLibelle3(?string $libelle3) : ApiFrGouvEnsapEvenementInterface
	{
		$this->_libelle3 = $libelle3;
		
		return $this;
	}
	
	/**
	 * Gets the third label of the event.
	 * 
	 * @return ?string
	 */
	public function getLibelle3() : ?string
	{
		return $this->_libelle3;
	}
	
	/**
	 * Sets the statut of the document. May be 'EVE_LU', meaning the document
	 * has been read, or 'EVE_NON_LU', meaning the document has not been read.
	 * 
	 * @param string $statut
	 * @return ApiFrGouvEnsapEvenementInterface
	 */
	public function setStatut(string $statut) : ApiFrGouvEnsapEvenementInterface
	{
		$this->_statut = $statut;
		
		return $this;
	}
	
	/**
	 * Gets the statut of the document. May be 'EVE_LU', meaning the document
	 * has been read, or 'EVE_NON_LU', meaning the document has not been read.
	 * 
	 * @return string
	 */
	public function getStatut() : string
	{
		return $this->_statut;
	}
	
	/**
	 * Sets the date when this document was read.
	 * 
	 * @param ?DateTimeInterface $dateLecture
	 * @return ApiFrGouvEnsapEvenementInterface
	 */
	public function setDateLecture(?DateTimeInterface $dateLecture) : ApiFrGouvEnsapEvenementInterface
	{
		$this->_dateLecture = $dateLecture;
		
		return $this;
	}
	
	/**
	 * Gets the date when this document was read.
	 * 
	 * @return ?DateTimeInterface
	 */
	public function getDateLecture() : ?DateTimeInterface
	{
		return $this->_dateLecture;
	}
	
	/**
	 * Sets the label for the icon of the event.
	 * 
	 * @param ?string $libelleIcone
	 * @return ApiFrGouvEnsapEvenementInterface
	 */
	public function setLibelleIcone(?string $libelleIcone) : ApiFrGouvEnsapEvenementInterface
	{
		$this->_libelleIcone = $libelleIcone;
		
		return $this;
	}
	
	/**
	 * Gets the label for the icon of the event.
	 * 
	 * @return ?string
	 */
	public function getLibelleIcone() : ?string
	{
		return $this->_libelleIcone;
	}
	
	/**
	 * Sets the id of the icon for the event.
	 * 
	 * @param ?string $icone
	 * @return ApiFrGouvEnsapEvenementInterface
	 */
	public function setIcone(?string $icone) : ApiFrGouvEnsapEvenementInterface
	{
		$this->_icone = $icone;
		
		return $this;
	}
	
	/**
	 * Gets the id of the icon for the event.
	 * 
	 * @return ?string
	 */
	public function getIcone() : ?string
	{
		return $this->_icone;
	}
	
	/**
	 * Sets the sort number ordering.
	 * 
	 * @param ?int $tri
	 * @return ApiFrGouvEnsapEvenementInterface
	 */
	public function setTri(?int $tri) : ApiFrGouvEnsapEvenementInterface
	{
		$this->_tri = $tri;
		
		return $this;
	}
	
	/**
	 * Gets the sort number ordering.
	 * 
	 * @return ?int
	 */
	public function getTri() : ?int
	{
		return $this->_tri;
	}
	
	/**
	 * Sets the action that should be launched wien clicked on this event.
	 * 
	 * @param string $actionIhm
	 * @return ApiFrGouvEnsapEvenementInterface
	 */
	public function setActionIhm(string $actionIhm) : ApiFrGouvEnsapEvenementInterface
	{
		$this->_actionIhm = $actionIhm;
		
		return $this;
	}
	
	/**
	 * Gets the action that should be launched wien clicked on this event.
	 * 
	 * @return string
	 */
	public function getActionIhm() : string
	{
		return $this->_actionIhm;
	}
	
	/**
	 * Sets the service of the event.
	 * 
	 * @param string $service
	 * @return ApiFrGouvEnsapEvenementInterface
	 */
	public function setService(string $service) : ApiFrGouvEnsapEvenementInterface
	{
		$this->_service = $service;
		
		return $this;
	}
	
	/**
	 * Gets the service of the event.
	 * 
	 * @return string
	 */
	public function getService() : string
	{
		return $this->_service;
	}
	
}
