<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-ensap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvEnsap;

use InvalidArgumentException;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * EnsapHttpCookieHandler class file.
 * 
 * This class represents an handler that handles the specifics of the ensap
 * cookie management.
 * 
 * @author Anastaszor
 */
class ApiFrGouvEnsapHttpCookieHandler
{
	
	/**
	 * The session cookies if we are logged in.
	 *
	 * @var array<string, string>
	 */
	protected array $_sessionCookies = [];
	
	/**
	 * Builds a new EnsapHttpCookieHandler with the default cookies.
	 * 
	 * @param array<string, string> $cookies
	 */
	public function __construct(array $cookies = [])
	{
		$this->_sessionCookies = $cookies;
	}
	
	/**
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Handles the request.
	 * 
	 * @param RequestInterface $request
	 * @return RequestInterface
	 */
	public function handleRequest(RequestInterface $request) : RequestInterface
	{
		if(!empty($this->_sessionCookies))
		{
			try
			{
				$request = $request->withHeader('Cookie', $this->getCookieStr());
			}
			// @codeCoverageIgnoreStart
			catch(InvalidArgumentException $e)
			// @codeCoverageIgnoreEnd
			{
				// nothing to do
			}
		}
		
		if(isset($this->_sessionCookies['XSRF-TOKEN']))
		{
			try
			{
				$request = $request->withHeader('X-XSRF-TOKEN', $this->_sessionCookies['XSRF-TOKEN']);
			}
			// @codeCoverageIgnoreStart
			catch(InvalidArgumentException $e)
			// @codeCoverageIgnoreEnd
			{
				// nothing to do
			}
		}
		
		return $request;
	}
	
	/**
	 * Handles the response.
	 * 
	 * @param ResponseInterface $response
	 * @return ResponseInterface
	 */
	public function handleResponse(ResponseInterface $response) : ResponseInterface
	{
		$cookies = $response->getHeader('Set-Cookie');
		
		foreach($cookies as $cookie)
		{
			$matches = [];
			if(\preg_match('#lemonap=(\\w+);#', $cookie, $matches))
			{
				/** @phpstan-ignore-next-line */
				$this->_sessionCookies['lemonap'] = $matches[1] ?? '';
			}
			
			$matches = [];
			if(\preg_match('#XSRF-TOKEN=(\\w+);#', $cookie, $matches))
			{
				/** @phpstan-ignore-next-line */
				$this->_sessionCookies['XSRF-TOKEN'] = $matches[1] ?? '';
			}
		}
		
		return $response;
	}
	
	/**
	 * Gets the string that represents actual known cookies.
	 *
	 * @return string
	 */
	public function getCookieStr() : string
	{
		$data = [];
		
		foreach($this->_sessionCookies as $key => $value)
		{
			$data[] = ((string) $key).'='.$value;
		}
		
		return \implode('; ', $data);
	}
	
}
