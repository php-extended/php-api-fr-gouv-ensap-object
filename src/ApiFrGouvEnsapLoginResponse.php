<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-ensap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvEnsap;

/**
 * ApiFrGouvEnsapLoginResponse class file.
 * 
 * This is a simple implementation of the ApiFrGouvEnsapLoginResponseInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 * @SuppressWarnings("PHPMD.ShortVariable")
 */
class ApiFrGouvEnsapLoginResponse implements ApiFrGouvEnsapLoginResponseInterface
{
	
	/**
	 * The error code of the login.
	 * 
	 * @var string
	 */
	protected string $_error;
	
	/**
	 * The hash if of the connection.
	 * 
	 * @var string
	 */
	protected string $_id;
	
	/**
	 * The code of the response.
	 * 
	 * @var int
	 */
	protected int $_code;
	
	/**
	 * The message of the response.
	 * 
	 * @var string
	 */
	protected string $_message;
	
	/**
	 * The result of the response.
	 * 
	 * @var int
	 */
	protected int $_result;
	
	/**
	 * Constructor for ApiFrGouvEnsapLoginResponse with private members.
	 * 
	 * @param string $error
	 * @param string $id
	 * @param int $code
	 * @param string $message
	 * @param int $result
	 */
	public function __construct(string $error, string $id, int $code, string $message, int $result)
	{
		$this->setError($error);
		$this->setId($id);
		$this->setCode($code);
		$this->setMessage($message);
		$this->setResult($result);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the error code of the login.
	 * 
	 * @param string $error
	 * @return ApiFrGouvEnsapLoginResponseInterface
	 */
	public function setError(string $error) : ApiFrGouvEnsapLoginResponseInterface
	{
		$this->_error = $error;
		
		return $this;
	}
	
	/**
	 * Gets the error code of the login.
	 * 
	 * @return string
	 */
	public function getError() : string
	{
		return $this->_error;
	}
	
	/**
	 * Sets the hash if of the connection.
	 * 
	 * @param string $id
	 * @return ApiFrGouvEnsapLoginResponseInterface
	 */
	public function setId(string $id) : ApiFrGouvEnsapLoginResponseInterface
	{
		$this->_id = $id;
		
		return $this;
	}
	
	/**
	 * Gets the hash if of the connection.
	 * 
	 * @return string
	 */
	public function getId() : string
	{
		return $this->_id;
	}
	
	/**
	 * Sets the code of the response.
	 * 
	 * @param int $code
	 * @return ApiFrGouvEnsapLoginResponseInterface
	 */
	public function setCode(int $code) : ApiFrGouvEnsapLoginResponseInterface
	{
		$this->_code = $code;
		
		return $this;
	}
	
	/**
	 * Gets the code of the response.
	 * 
	 * @return int
	 */
	public function getCode() : int
	{
		return $this->_code;
	}
	
	/**
	 * Sets the message of the response.
	 * 
	 * @param string $message
	 * @return ApiFrGouvEnsapLoginResponseInterface
	 */
	public function setMessage(string $message) : ApiFrGouvEnsapLoginResponseInterface
	{
		$this->_message = $message;
		
		return $this;
	}
	
	/**
	 * Gets the message of the response.
	 * 
	 * @return string
	 */
	public function getMessage() : string
	{
		return $this->_message;
	}
	
	/**
	 * Sets the result of the response.
	 * 
	 * @param int $result
	 * @return ApiFrGouvEnsapLoginResponseInterface
	 */
	public function setResult(int $result) : ApiFrGouvEnsapLoginResponseInterface
	{
		$this->_result = $result;
		
		return $this;
	}
	
	/**
	 * Gets the result of the response.
	 * 
	 * @return int
	 */
	public function getResult() : int
	{
		return $this->_result;
	}
	
}
