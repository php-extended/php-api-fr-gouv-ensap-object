<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-api-fr-gouv-ensap-object library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\ApiFrGouvEnsap;

/**
 * ApiFrGouvEnsapIdentification class file.
 * 
 * This is a simple implementation of the
 * ApiFrGouvEnsapIdentificationInterface.
 * 
 * /!\ This file was generated automatically from the json-schema.json file.
 * /!\ Do not edit by hand or the modifications will be erased.
 * @generator PhpExtended\JsonSchema\Php74ClassMetadata
 * 
 * @author Anastaszor
 */
class ApiFrGouvEnsapIdentification implements ApiFrGouvEnsapIdentificationInterface
{
	
	/**
	 * The identity of the user.
	 * 
	 * @var string
	 */
	protected string $_identite;
	
	/**
	 * Constructor for ApiFrGouvEnsapIdentification with private members.
	 * 
	 * @param string $identite
	 */
	public function __construct(string $identite)
	{
		$this->setIdentite($identite);
	}
	
	/**
	 * {@inheritDoc}
	 * @see \Stringable::__toString()
	 */
	public function __toString() : string
	{
		return static::class.'@'.\spl_object_hash($this);
	}
	
	/**
	 * Sets the identity of the user.
	 * 
	 * @param string $identite
	 * @return ApiFrGouvEnsapIdentificationInterface
	 */
	public function setIdentite(string $identite) : ApiFrGouvEnsapIdentificationInterface
	{
		$this->_identite = $identite;
		
		return $this;
	}
	
	/**
	 * Gets the identity of the user.
	 * 
	 * @return string
	 */
	public function getIdentite() : string
	{
		return $this->_identite;
	}
	
}
