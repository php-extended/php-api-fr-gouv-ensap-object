<?php

use PhpExtended\ApiFrGouvEnsap\ApiFrGouvEnsapPdfParser;

/*
 * Example script on how to parse bulletin files.
 * This script launch from command line needs one argument :
 * - A valid path on a local pdf file that is a bulletin pdf
 * 
 * This script will parse the local file and output all the details of the
 * bulletin that were found.
 */

global $argv;

if(!isset($argv[1]))
{
	throw new InvalidArgumentException('The first argument should be the local path of the pdf file');
}
$realpath = realpath($argv[1]);
if($realpath === false)
{
	throw new InvalidArgumentException('The given path does not point to an existing file at '.$argv[1]);
}
if(!is_file($realpath))
{
	throw new InvalidArgumentException('The given path does not point to a file at '.$realpath);
}
if(!is_readable($realpath))
{
	throw new InvalidArgumentException('The given file is not readable at '.$realpath);
}

$composer = __DIR__.'/vendor/autoload.php';
if(!is_file($composer))
{
	throw new LogicException('The composer vendor directory is not up. Please run composer first.');
}
require_once $composer;

$parser = new ApiFrGouvEnsapPdfParser();

try
{
	$bulletin = $parser->parseBulletinFromFile($realpath);
}
catch(\Throwable $exc)
{
	throw new RuntimeException('Failed to parse bulletin from file contents from '.$realpath, -1, $exc);
}

echo json_encode($bulletin, JSON_PRETTY_PRINT);
