# php-extended/php-api-fr-gouv-ensap-object
A php API wrapper to connect to ensap.gouv.fr instances

![coverage](https://gitlab.com/php-extended/php-api-fr-gouv-ensap-object/badges/master/pipeline.svg?style=flat-square)
![build status](https://gitlab.com/php-extended/php-api-fr-gouv-ensap-object/badges/master/coverage.svg?style=flat-square)


## Installation

The installation of this library is made via composer and the autoloading of
all classes of this library is made through their autoloader.

- Download `composer.phar` from [their website](https://getcomposer.org/download/).
- Then run the following command to install this library as dependency :
- `php composer.phar php-extended/php-api-fr-gouv-ensap-object ^8`


## Basic Usage

This library may be used the following way. First, we need to log in, this needs
the NIR number (no_secu) and the associated password.

```php

use PhpExtended\EnsapGouvFr\EnsapApiEndpoint;

/* @var $client \Psr\Http\Client\ClientInterface */
/* @var $uriFactory \Psr\Http\Message\UriFactory */
/* @var $requestFactory \Psr\Http\Message\RequestFactory */

$endpoint = new EnsapApiEndpoint($client, $uriFactory, $requestFactory);

// first, we need to log in
$endpoint->login('<no_secu>', '<password>');

```

Next, you can retrieve the list of recent events :

```php

$endpoint->getDataAccueilConnecte();	// returns EnsapApiAccueilConnecte object

```

Or you can retreive the list of documents for your remuneration :

```php

$endpoint->getDataRemuneration();		// returns EnsapApiRemuneration object

```

Finally, you can get the raw data of the documents in both objects by using their uuid :

```php

$endpoint->getRawDocument($documentUuid);	// returns EnsapApiRawDocument object

```

There also exists an experimental parser for bulletin pdf files.

```php

$endpoint->getBulletinFromRawDocument(EnsapApiRawDocument $document);	// returns EnsapApiBulletin object

```

As this parser is experimental, it may fail (and throw `\RuntimeException`s) even
if the pdf is a perfectly human readable pdf file. Test it on your pdf files and
give me a pull request for it to be improved !

Wants to be working with the versions of the pdf generator :
- PAY18E - V1.4 - 25102016 (since dec 2016)
- PAY18E - V1.6 - 04122017 (since feb 2018)
- PAY18E - V2.0 - 26062018 (since oct 2018)
- PAY18E - V2.1 - 28112018 (since dec 2018)
- PAY18E - V2.3 - 29032019 (since apr 2019)


## License

MIT (See [license file](LICENSE)).
